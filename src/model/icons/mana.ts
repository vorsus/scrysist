export class CardColorIndicators {
  static iconClass(colors: string[] | null | undefined): string {
    if (!colors || colors.length == 0 || colors[0] === 'C') {
      return 'ms ms-fw ms-dfc-ignite';
    }

    return 'ms ms-fw ms-ci ms-ci-' + colors.length + ' ms-ci-' + colors.map((s) => s.toLowerCase()).join('');
  }
  static normalize(colors: string[]): string[] {
    return colors.sort();
  }
}

export const cardTypeIcons = {
  lands: 'ms ms-fw ms-land',
  creatures: 'ms ms-fw ms-creature',
  instants: 'ms ms-fw ms-instant',
  sorceries: 'ms ms-fw ms-sorcery',
  artifacts: 'ms ms-fw ms-artifact',
  enchantments: 'ms ms-fw ms-enchantment',
  planeswalkers: 'ms ms-fw ms-planeswalker',
  battles: 'ms ms-fw ms-battle',
  others: 'ms ms-fw ms-scheme'
};
