import MagicTag from '../types/MagicTag';

class Tags {
  _tags: { [index: string]: MagicTag } = {};

  get(tagName: string): MagicTag {
    return this._tags[tagName];
  }

  add(tag: MagicTag): void {
    this._tags[tag.name] = tag;
  }

  addList(tags: MagicTag[]): void {
    for (const tag of tags) {
      this.add(tag);
    }
  }

  has(tagName: string): boolean {
    return this._tags[tagName] !== undefined;
  }

  getList(): MagicTag[] {
    return Object.values(this._tags);
  }

  getSortedList(): MagicTag[] {
    const list: MagicTag[] = Object.values(this._tags);
    list.sort((tagA, tagB) => {
      if (tagA.name > tagB.name) {
        return 1;
      }
      if (tagA.name < tagB.name) {
        return -1;
      }
      return 0;
    });

    return list;
  }
}

export default Tags;
