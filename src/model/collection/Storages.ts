import StorageAdapter from '../types/StorageAdapter';
import { StorageFile, StorageSlot } from '../interfaces/Storage';

class Storages {
  _decks: { [index: string]: StorageAdapter } = {};

  getData(path: string): StorageFile {
    return <StorageFile>(<unknown>this._decks[path].data());
  }

  getDataList(): { [index: string]: StorageFile } {
    const ds: { [index: string]: StorageFile } = {};
    for (const k in this._decks) {
      ds[k] = this.getData(k);
    }

    return ds;
  }

  add(deck: StorageAdapter): void {
    this._decks[deck.getPath()] = deck;
  }

  has(path: string): boolean {
    return this._decks[path] !== undefined;
  }

  setField(path: string, key: string, value: string | string[] | StorageSlot[] | number | null): void {
    this._decks[path].setField(key, value);
  }

  sync(path: string): void {
    this._decks[path].sync();
  }
}

export default Storages;
