import MagicEdition from '../types/MagicEdition';

class Editions {
  _editions: { [index: string]: MagicEdition } = {};

  get(code: string): MagicEdition {
    return this._editions[code];
  }

  add(edn: MagicEdition): void {
    this._editions[edn.code] = edn;
  }

  has(code: string): boolean {
    return this._editions[code] !== undefined;
  }

  getList(): MagicEdition[] {
    return Object.values(this._editions);
  }
}

export default Editions;
