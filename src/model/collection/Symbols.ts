import MagicSymbol from '../types/MagicSymbol';

class Symbols {
  _symbols: { [index: string]: MagicSymbol } = {};

  get(code: string): MagicSymbol {
    return this._symbols[code];
  }

  add(sym: MagicSymbol): void {
    this._symbols[sym.code] = sym;
  }

  getList(): MagicSymbol[] {
    return Object.values(this._symbols);
  }
}

export default Symbols;
