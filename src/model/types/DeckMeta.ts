import { DeckFlag } from '@model/enums';
import { StorageFile } from '../interfaces/Storage';

class DeckMeta {
  [index: string]: string | string[] | number | number[];
  level = 5;
  colors: string[] = [];
  flags: DeckFlag[] = [];
  formats: string[] = ['kitchen'];
  type = 'normal';
  comment = '';
  signatures: string[] = [];
  companions: string[] = [];
  generals: string[] = [];
  tags: string[] = [];
  years: number[] = [];

  static fromStorage(storageMeta: StorageFile): DeckMeta {
    const meta = new DeckMeta();
    meta.level = storageMeta.level ?? 5;
    meta.colors = storageMeta.colors ?? [];
    meta.formats = storageMeta.formats ?? ['kitchen'];
    meta.flags = storageMeta.flags ?? [];
    meta.type = storageMeta.type ?? 'normal';
    meta.comment = storageMeta.comment ?? '';
    meta.signatures = storageMeta.signatures ?? [];
    meta.companions = storageMeta.companions ?? [];
    meta.generals = storageMeta.generals ?? [];
    meta.tags = storageMeta.tags ?? [];
    meta.years = storageMeta.years ?? [];

    return meta;
  }
}

export default DeckMeta;
