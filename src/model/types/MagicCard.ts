import { ActualColors, CardRarity } from '../enums';
import { CardPriceList } from '../interfaces/Scryfall';

class MagicCard {
  id = '';
  mtgoId = '';
  counter = 0;
  otherNames: string[] = [];
  oracleTexts: string[] = [];
  costs: string[] = [];
  prices: CardPriceList = {};
  type = '';
  index = '';
  superTypes: string[] = [];
  subTypes: string[] = [];
  isCreature = false;
  isPermanent = false;
  isLand = false;
  cost = '';
  colors: string[] = [];
  colorIdentity: ActualColors[] = [];
  producedMana: ActualColors[] = [];
  keywords: string[] = [];
  rarity: CardRarity = 'special';
  editionCode = '';
  editionName = '';
  language: string | undefined;

  power: string | undefined;
  toughness: string | undefined;
  loyalty: string | undefined;
  defense: string | undefined;
  scryfallUri: string | undefined;
  gathererUri: string | undefined;
  edhrecRank: number | undefined;

  imageUrlNormal: string | undefined;
  imageUrlSmall: string | undefined;
  imageUrlNormalBack: string | undefined;
  imageUrlSmallBack: string | undefined;
  imageUrlArt: string | undefined;

  // only for debugging
  inBooster = true;
  finishes: string[] = [];

  // on the fly
  cipNormal: string | undefined;
  cipSmall: string | undefined;
  cipArt: string | undefined;
  cipNormalBack: string | undefined;
  cipSmallBack: string | undefined;

  constructor(
    readonly oracleId: string,
    readonly englishName: string,
    readonly cmc: number
  ) {}
}

export default MagicCard;
