import { Board, CardRarity, CardType } from '../enums';
import { StorageSlot } from '../interfaces/Storage';
import MagicCard from './MagicCard';

class DeckSlot {
  name: string;
  oracleId: string;
  type: string;

  overrideCmc: number | null = null;
  comment = '';

  // virtual fields not stored
  rarity: CardRarity = 'special';
  editionCode = '';
  cost = '';
  cmc = 0;
  cmcString = '0';
  typeSortString = '';
  power = '';
  combinedDefense = '';
  cardGroup: CardType = 'others';

  constructor(
    readonly linkedCard: MagicCard,
    public quantity: number,
    public board: Board
  ) {
    this.oracleId = linkedCard.oracleId;
    this.name = linkedCard.englishName;
    this.type = linkedCard.type;

    this.appendVirtualFields();
  }

  getRealCmc(): number {
    if (null !== this.overrideCmc) {
      return this.overrideCmc;
    }

    return this.cmc;
  }

  reduce(): StorageSlot {
    return {
      name: this.name,
      oracleId: this.oracleId,
      type: this.type,
      quantity: this.quantity,
      comment: this.comment,
      board: this.board,
      overrideCmc: this.overrideCmc
    };
  }

  static fromStorage(storageSlot: StorageSlot, card: MagicCard): DeckSlot {
    const deckSlot = new DeckSlot(card, storageSlot.quantity, storageSlot.board ?? 'main');
    deckSlot.comment = storageSlot.comment ?? '';
    deckSlot.overrideCmc = storageSlot.overrideCmc ?? null;

    return deckSlot;
  }

  private appendVirtualFields(): void {
    // console.log(slot, slot.linkedCard);
    this.rarity = this.linkedCard.rarity;
    this.editionCode = this.linkedCard.editionCode;
    this.cost = this.linkedCard.cost;
    this.cmc = this.linkedCard.cmc;
    this.cmcString = this.linkedCard.cmc + ''; // sorting fails with integer

    // dirty fix, to make (x) costs after lands
    if (this.cmcString === '0') {
      if (this.cost.trim()) {
        this.cmcString = '0 ';
      }
    }

    this.power = this.linkedCard.power ?? '';
    this.combinedDefense = this.linkedCard.toughness ?? '';
    if (this.combinedDefense === '') {
      this.combinedDefense = this.linkedCard.loyalty ?? '';
    }
    if (this.combinedDefense === '') {
      this.combinedDefense = this.linkedCard.defense ?? '';
    }

    this.setCardGroup();
    this.setTypeSort();
  }

  private setCardGroup(): void {
    // define priority (creature > enchantment (enchantment creature); enchantment > artifact (enchantment artifact) etc.)
    const card = this.linkedCard;

    // TBD: Dryad Arbor / Urza's Saga as land?
    if (card.isLand) {
      this.cardGroup = 'lands';
    } else if (card.isCreature) {
      this.cardGroup = 'creatures';
    } else if (card.superTypes.includes('Instant')) {
      this.cardGroup = 'instants';
    } else if (card.superTypes.includes('Sorcery')) {
      this.cardGroup = 'sorceries';
    } else if (card.superTypes.includes('Planeswalker')) {
      this.cardGroup = 'planeswalkers';
    } else if (card.superTypes.includes('Enchantment')) {
      this.cardGroup = 'enchantments';
    } else if (card.superTypes.includes('Artifact')) {
      this.cardGroup = 'artifacts';
    } else if (card.superTypes.includes('Battle')) {
      this.cardGroup = 'battles';
    } // else others, just in case ...
  }

  private setTypeSort(): void {
    let prefix: string;
    const card = this.linkedCard;

    switch (this.cardGroup) {
      case 'lands':
        prefix = 'a';
        break;
      case 'artifacts':
        prefix = 'b';
        break;
      case 'creatures':
        prefix = 'c';
        break;
      case 'instants':
        prefix = 'd';
        break;
      case 'sorceries':
        prefix = 'e';
        break;
      case 'enchantments':
        prefix = 'f';
        break;
      case 'planeswalkers':
        prefix = 'g';
        break;
      default:
        prefix = 'h';
        break;
    }

    if (card.superTypes.includes('Legendary')) {
      prefix += 'c';
    } else if (card.superTypes.includes('Tribal')) {
      prefix += 'd';
    } else if (card.superTypes.includes('Basic')) {
      prefix += 'a';
    } else {
      prefix += 'b';
    }

    this.typeSortString = prefix + ' ' + this.type;
  }
}

export default DeckSlot;
