import DeckMeta from './DeckMeta';
import DeckSlot from './DeckSlot';
import DeckStats from './DeckStats';

class Deck {
  constructor(
    readonly meta: DeckMeta,
    readonly stats: DeckStats,
    readonly slots: DeckSlot[],
    readonly path: string
  ) {}
}

export default Deck;
