import { TagType } from '@model/enums';

class MagicTag {
  constructor(
    readonly name: string,
    readonly type: TagType,
    readonly auto = false
  ) {}
}

export default MagicTag;

export const formatTag = (tagName: string): string => {
  return tagName.charAt(0).toUpperCase() + tagName.slice(1).toLowerCase();
};
