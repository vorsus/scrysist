import { CardType, ManaColor, ActualColors } from '../enums';
import { StrNum } from '../interfaces/KeyPairs';

class DeckStats {
  price = Number(0);
  avgEdhrecRank = Number(0);

  colors: StrNum = {
    C: 0,
    B: 0,
    G: 0,
    R: 0,
    U: 0,
    W: 0,

    BG: 0,
    BR: 0,
    BU: 0,
    BW: 0,
    GR: 0,
    GU: 0,
    GW: 0,
    RU: 0,
    RW: 0,
    UW: 0,

    BGR: 0,
    BGU: 0,
    BGW: 0,
    BRU: 0,
    BRW: 0,
    BUW: 0,
    GRU: 0,
    GRW: 0,
    GUW: 0,
    RUW: 0,

    C4: 0,
    C5: 0
  };

  manaProduction: { [index in ManaColor]: number } = {
    B: 0,
    G: 0,
    R: 0,
    U: 0,
    W: 0,
    C: 0
  };

  colorIdentity: { [index in ActualColors]: number } = {
    B: 0,
    G: 0,
    R: 0,
    U: 0,
    W: 0
  };

  types: { [index in CardType]: StrNum } = {
    creatures: {},
    enchantments: {},
    instants: {},
    sorceries: {},
    planeswalkers: {},
    battles: {},
    artifacts: {},
    lands: {},
    others: {}
  };

  total: { [index in CardType]: number } = {
    creatures: 0,
    enchantments: 0,
    instants: 0,
    sorceries: 0,
    planeswalkers: 0,
    battles: 0,
    artifacts: 0,
    lands: 0,
    others: 0
  };

  cmc: { [index: number]: { c: number; nc: number; s: number } } = {
    0: { c: 0, nc: 0, s: 0 },
    1: { c: 0, nc: 0, s: 0 },
    2: { c: 0, nc: 0, s: 0 },
    3: { c: 0, nc: 0, s: 0 },
    4: { c: 0, nc: 0, s: 0 },
    5: { c: 0, nc: 0, s: 0 },
    6: { c: 0, nc: 0, s: 0 },
    7: { c: 0, nc: 0, s: 0 }
  };

  getTotal(): number {
    return (
      this.total.creatures +
      this.total.enchantments +
      this.total.instants +
      this.total.sorceries +
      this.total.planeswalkers +
      this.total.battles +
      this.total.artifacts +
      this.total.lands
    );
  }
}

export default DeckStats;
