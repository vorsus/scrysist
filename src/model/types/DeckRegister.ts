import { Board, ManaColor } from '../enums';

class DeckRegister {
  oracleIds: string[] = [];
  level = 5;
  colors: ManaColor[] = [];
  tags: string[] = [];
  formats: string[] = ['kitchen'];
  type = 'normal';
  companions: string[] = [];
  slots: { name: string; oracleId: string; quantity: number; board: Board }[] = [];

  constructor(
    readonly path: string,
    public ts: number
  ) {}
}

export default DeckRegister;
