import MagicCard from './MagicCard';
import DeckSlot from './DeckSlot';

class PlayLibrary {
  public library: MagicCard[] = [];
  public hand: MagicCard[] = [];
  public battleField: MagicCard[] = [];

  private cards: MagicCard[] = [];

  constructor(slots: DeckSlot[]) {
    slots.forEach((slot) => {
      for (let i = 0; i < slot.quantity; i++) {
        this.cards.push(slot.linkedCard);
      }
    });
  }

  public restart(count = 7): void {
    this.hand = [];
    this.library = [];
    this.battleField = [];
    for (const card of this.cards) {
      this.library.push(card);
    }

    this.shuffle();
    this.drawCards(count);
  }

  public shuffle(): void {
    this.library.sort(() => {
      return 0.5 - Math.random();
    });
  }

  public drawCards(count: number): void {
    for (let i = 0; i < count; i++) {
      const card = this.library.shift();
      if (card) {
        this.hand.push(card);
      }
    }
  }

  public moveFromHand(index: number): void {
    const card = this.hand[index];
    delete this.hand[index];
    this.hand = Object.values(this.hand);
    this.battleField.push(card);
  }
}

export default PlayLibrary;
