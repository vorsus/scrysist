import { Currency } from '../enums';

class AppSettings {
  constructor(
    public basePath: string = '',
    public openLimit: number = 50,
    public currency: Currency = 'eur'
  ) {}
}

export default AppSettings;
