class MagicSymbol {
  constructor(
    readonly code: string,
    readonly name: string,
    readonly svgUri: string,
    readonly cip: string
  ) {}
}

export default MagicSymbol;
