import JSONdb from 'simple-json-db';
import { StorageFile, StorageSlot } from '../interfaces/Storage';

class StorageAdapter {
  #path: string;
  #db: JSONdb;

  constructor(path: string, db: JSONdb) {
    this.#path = path;
    this.#db = db;
  }

  getPath(): string {
    return this.#path;
  }

  setField(key: string, value: string | string[] | StorageSlot[] | number | null): void {
    this.#db.set(key, value);
  }

  sync(): void {
    this.#db.sync();
  }

  data(): StorageFile {
    return <StorageFile>(<unknown>this.#db.JSON());
  }
}

export default StorageAdapter;
