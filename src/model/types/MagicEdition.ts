class MagicEdition {
  constructor(
    readonly id: string,
    readonly code: string,
    readonly name: string,
    readonly cardCount: number,
    readonly releasedAt: string,
    readonly scryfallUri: string,
    readonly type: string,
    readonly svgUri: string,
    readonly cip: string
  ) {}
}

export default MagicEdition;
