import { Board, DeckFlag, ManaColor } from '../enums';

export type StorageValues = string | string[] | StorageSlot[] | number | null;

export interface StorageFile {
  slots: StorageSlot[];

  // maybe missing on first load
  level: number | undefined;
  colors: ManaColor[] | undefined;
  formats: string[] | undefined;
  type: string | undefined;
  flags: DeckFlag[] | undefined;
  comment: string | undefined;
  signatures: string[] | undefined;
  companions: string[] | undefined;
  generals: string[] | undefined;
  tags: string[] | undefined;
  years: number[] | undefined;
}

export interface StorageSlot {
  name: string;
  quantity: number;

  // maybe missing on first load
  oracleId: string | undefined;
  type: string | undefined;
  comment: string | undefined;
  board: Board | undefined;
  overrideCmc: number | null | undefined;
}

export interface FileTreeNodeBase {
  key: string;
  leaf: boolean;
  label: string;
}

export interface FileTreeNode extends FileTreeNodeBase {
  icon: string;
  children: FileTreeNode[];
  colors: string[];
  flags: DeckFlag[];
  expandedIcon: boolean;
  collapsedIcon: boolean;
  isSpecial: boolean;
}
