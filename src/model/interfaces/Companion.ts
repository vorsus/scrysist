import MagicCard from '../types/MagicCard';

export interface Companion {
  oracleId: string;
  name: string;
  card: MagicCard;
  badge?: string;
}
