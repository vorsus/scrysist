import MagicTag from '../types/MagicTag';
import { FileTreeNode } from './Storage';

export type BoolCallback = (state: boolean) => void;
export type DeckLoadedCallback = (state: boolean, root: string, tree: FileTreeNode[], flatten: string[], tagList: MagicTag[]) => void;
