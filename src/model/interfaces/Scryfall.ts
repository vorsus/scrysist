import { ActualColors, CardRarity } from '../enums';

type ImageUris = {
  normal: string;
  small: string;
  art_crop: string;
};

type CardFace = {
  image_uris: ImageUris;
  colors: undefined | string[];
  oracle_text: undefined | string;
  mana_cost: string;
  power?: string;
  toughness?: string;
  loyalty?: string;
  defense?: string;
};

export type CardPriceList = {
  [Currency in string]: string;
};

export interface ScryfallCard extends CardFace {
  id: string;
  oracle_id: string;
  set: string;
  set_name: string;
  rarity: CardRarity;
  colors: undefined | string[];
  produced_mana: undefined | ActualColors[];
  type_line: string;
  scryfall_uri: string;
  related_uris: { gatherer: string };
  booster: boolean;
  finishes: string[];
  card_faces: undefined | CardFace[];
  color_identity: ActualColors[];
  name: string;
  cmc: number;
  keywords: string[];
  mana_cost: string;
  flavor_name: undefined | string;
  edhrec_rank: number;
  prices: CardPriceList;
  digital: boolean;
  oversized: boolean;
  layout: string;
  lang: string;
}

export interface SryfallSet {
  id: string;
  scryfall_uri: string;
  code: string;
  name: string;
  set_type: string;
  card_count: number;
  digital: boolean;
  released_at: string;
  icon_svg_uri: string;
}

export interface ScryfallSymbol {
  symbol: string;
  english: string;
  svg_uri: string;
}
