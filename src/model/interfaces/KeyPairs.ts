export type StrStr = { [index: string]: string };
export type StrNum = { [index: string]: number };
export type StrBool = { [index: string]: boolean };
