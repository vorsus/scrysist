import { StrStr } from '../interfaces/KeyPairs';

export type Board = 'main' | 'side' | 'consider';
export type Currency = 'none' | 'eur' | 'usd';
export type TagType = 'custom' | 'keyword' | 'card type' | 'tribal' | 'color';
export type CardType = 'creatures' | 'enchantments' | 'instants' | 'sorceries' | 'planeswalkers' | 'battles' | 'artifacts' | 'lands' | 'others';
export type ManaColor = 'B' | 'G' | 'R' | 'U' | 'W' | 'C';
export type ActualColors = 'B' | 'G' | 'R' | 'U' | 'W';
export type CardRarity = 'special' | 'common' | 'uncommon' | 'bonus' | 'rare' | 'mythic';
export type DeckFlag = 'construction' | 'favorite' | 'bookmark';

export const CARD_GROUPS = new Set<CardType>([
  'planeswalkers',
  'creatures',
  'instants',
  'sorceries',
  'enchantments',
  'artifacts',
  'battles',
  'others',
  'lands'
]);

export const SPECIAL_FOLDERS: StrStr = {
  DRAFT: 'Draft'
};

export const FORMATS: StrStr = {
  KITCHEN: 'kitchen',
  STANDARD: 'standard',
  PIONEER: 'pioneer',
  MODERN: 'modern',
  LEGACY: 'legacy',
  VINTAGE: 'vintage',
  PAUPER: 'pauper',
  COMMANDER: 'commander'
};

export const DECK_TYPES: StrStr = {
  NORMAL: 'normal',
  EDH: 'edh',
  BRAWL: 'brawl',
  OATHBREAKER: 'oathbreaker',
  DRAFT: 'draft',
  OTHER: 'other'
};

// main: min, max | side: max | unique cards: max
export const DECK_BOARD_SIZES: { [index: string]: number[] } = {
  normal: [60, 999, 15, 4],
  edh: [100, 100, 0, 1],
  brawl: [60, 60, 0, 1],
  oathbreaker: [60, 60, 0, 1],
  draft: [40, 999, 999, 999],
  other: [1, 999, 999, 999]
};

export const isColorIdentityRequired = (type: string): boolean => {
  return type === DECK_TYPES.EDH || type === DECK_TYPES.OATHBREAKER || type === DECK_TYPES.BRAWL;
};

export const BASIC_LANDS: StrStr = {
  Swamp: '02cb5cfd-018e-4c5e-bef1-166262aa5f1d',
  Mountain: '53fb7b99-9e47-46a6-9c8a-88e28b5197f1',
  Plains: 'a9891b7b-fc52-470c-9f74-292ae665f378',
  Island: 'acf7b664-3e75-4018-81f6-2a14ab59f258',
  Forest: '32af9f41-89e2-4e7a-9fec-fffe79cae077'
};

export const COMPANIONS: StrStr = {
  Lurrus: '5ad36fb2-c44e-4085-ba0d-54277841ad3a',
  Umori: '75ac31e0-ac70-4ee6-b2b1-cc445ffa1da9',
  Jegantha: '1d52e527-3835-4350-8c01-0f2d5d623b9c',
  Obosh: '451507de-9c42-43ee-b9ba-1f69e9aa29d2',
  Gyruda: '97eb1804-6fd8-4917-af36-87fdfce39d3a',
  Kaheera: 'd4ebed0b-8060-4a7b-a060-5cfcd2172b16',
  Lutri: 'fb1189c9-7842-466e-8238-1e02677d8494',
  Zirda: '1bd8e61c-2ee8-4243-a848-7008810db8a0',
  Keruga: 'a90ee952-de7a-420f-993c-a38db89bc8ac',
  Yorion: '275426c4-c14e-47d0-a9d4-24da7f6f6911'
};

export const TAG_TYPES: { [index: string]: TagType } = {
  CARD_TYPE: 'card type',
  TRIBAL: 'tribal',
  KEYWORD: 'keyword',
  COLOR: 'color',
  CUSTOM: 'custom'
};

export const PLAIN_COLORS: StrStr = {
  B: 'Black',
  G: 'Green',
  R: 'Red',
  U: 'Blue',
  W: 'White'
};

export const COLOR_NAMES: StrStr = {
  B: 'Mono Black',
  G: 'Mono Green',
  R: 'Mono Red',
  U: 'Mono Blue',
  W: 'Mono White',
  C: 'Colorless',

  BG: 'Golgari',
  BR: 'Rakdos',
  BU: 'Dimir',
  BW: 'Orzhov',
  GR: 'Gruul',
  GU: 'Simic',
  GW: 'Selesnya',
  RU: 'Izzet',
  RW: 'Boros',
  UW: 'Azorius',

  BGR: 'Jund',
  BGU: 'Sultai',
  BGW: 'Abzan',
  BRU: 'Grixis',
  BRW: 'Mardu',
  BUW: 'Esper',
  GRU: 'Temur',
  GRW: 'Naya',
  GUW: 'Bant',
  RUW: 'Jeskai'
};

export const isBasicLand = (cardName: string): boolean => {
  return (
    cardName === 'Forest' ||
    cardName === 'Swamp' ||
    cardName === 'Plains' ||
    cardName === 'Mountain' ||
    cardName === 'Island' ||
    cardName === 'Snow-Covered Forest' ||
    cardName === 'Snow-Covered Swamp' ||
    cardName === 'Snow-Covered Plains' ||
    cardName === 'Snow-Covered Mountain' ||
    cardName === 'Snow-Covered Island'
  );
};

export const isSpecialPath = (deckPath: string, rootPath: string, sep: string): boolean => {
  return (
    1 === deckPath.substring(rootPath.length).indexOf(SPECIAL_FOLDERS.DRAFT + sep) ||
    (1 === deckPath.substring(rootPath.length).indexOf(SPECIAL_FOLDERS.DRAFT) && deckPath.length === rootPath.length + 1 + SPECIAL_FOLDERS.DRAFT.length)
  );
};
