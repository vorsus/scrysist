import { defineStore } from 'pinia';
import MagicTag from '@model/types/MagicTag';
import Tags from '@model/collection/Tags';
import { FileTreeNode } from '@model/interfaces/Storage';

export type FilesState = {
  deckFileTree: FileTreeNode[];
  deckFileRoot: string;
  flattenFilePaths: string[];
  customTagList: Tags;
  autoTagList: MagicTag[];
};

export const useFilesStore = defineStore('files', {
  state: () =>
    ({
      deckFileTree: [],
      deckFileRoot: '',
      flattenFilePaths: [],
      customTagList: new Tags(),
      autoTagList: []
    }) as FilesState,
  actions: {}
});
