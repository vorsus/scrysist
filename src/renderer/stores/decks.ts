import { defineStore } from 'pinia';
import Deck from '@model/types/Deck';
import EventHandler from '@renderer/utils/eventHandler';
import { StrStr } from '@model/interfaces/KeyPairs';
import { extractTitle } from '@renderer/utils/formatHelper';

const openLimit = window.appService.getOpenLimit();

export type DecksState = {
  activeDeckTableTab: number | undefined;
  activeDeckTablePath: string | null;
  deckTableTabs: StrStr;
  loadedDecks: { [index: string]: Deck | undefined };
};

export const useDecksStore = defineStore('decks', {
  state: () =>
    ({
      activeDeckTableTab: undefined,
      activeDeckTablePath: null,
      deckTableTabs: {},
      loadedDecks: {}
    }) as DecksState,
  actions: {
    async loadDeck(path: string, deck: Deck): Promise<void> {
      const lengthBefore = Object.keys(this.deckTableTabs).length;

      if (lengthBefore >= openLimit) {
        EventHandler.emit('show-warning', 'Open decks limit of ' + openLimit + ' reached!');
        return;
      }

      this.deckTableTabs[path] = extractTitle(path);
      this.loadedDecks[path] = deck;
      this.activeDeckTableTab = lengthBefore; // last index
    },
    async unloadDeck(path: string) {
      delete this.deckTableTabs[path];
      this.loadedDecks[path] = undefined;
    }
  }
});
