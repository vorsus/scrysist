import { defineStore } from 'pinia';
import MagicCard from '@model/types/MagicCard';
import AppSettings from '@model/types/AppSettings';
import { Companion } from '@model/interfaces/Companion';

type PreviewTabs = 'info' | 'decks' | 'links' | 'checks';

export type ViewState = {
  currentPreviewCard: MagicCard | null;
  searchInput: string;
  searchResults: MagicCard[];
  companionCards: Companion[];
  draggedSearchCard: MagicCard | null;
  appSettings: AppSettings | undefined;
  hideCommon: boolean;
  hideSearch: boolean;
  hideFileTree: boolean;
  hidePreview: boolean;
  drawSidebar: boolean;
  defaultDeckPanel: string;
  activeTableView: string;
  activePreviewTab: PreviewTabs;
  currentTheme: string;
};

export const useViewStore = defineStore('view', {
  state: () =>
    ({
      currentPreviewCard: null,
      searchInput: '',
      searchResults: [],
      companionCards: [],
      draggedSearchCard: null,
      appSettings: undefined,
      hideCommon: false,
      hideSearch: false,
      hideFileTree: false,
      hidePreview: false,
      drawSidebar: false,
      defaultDeckPanel: 'mb',
      activeTableView: 'standard',
      currentTheme: 'naon',
      activePreviewTab: 'info'
    }) as ViewState,
  actions: {
    showPreview(card: MagicCard | null, force = true): void {
      if (card && (force || !this.hidePreview)) {
        this.currentPreviewCard = card;
        this.hidePreview = false;
      }
    },
    showPreviewOracleId(oracleId: string, force = true): void {
      const card = window.modelService.Cards.byOracleId(oracleId);
      this.showPreview(card, force);
    }
  }
});
