import { defineStore } from 'pinia';
import PlayLibrary from '@model/types/PlayLibrary';

export type PlaysState = {
  playLibraries: { [index: string]: PlayLibrary | undefined };
};

export const usePlaysStore = defineStore('plays', {
  state: () =>
    ({
      playLibraries: {}
    }) as PlaysState,
  actions: {}
});
