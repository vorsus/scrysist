import { createApp } from 'vue';
import { createPinia } from 'pinia';

import App from './App.vue';
import PrimeVue from 'primevue/config';
import Tooltip from 'primevue/tooltip';
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';
import BadgeDirective from 'primevue/badgedirective';
import VueApexCharts from 'vue3-apexcharts';

import 'primevue/resources/primevue.min.css';
import 'primeflex/primeflex.css';
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css';
import 'mana-font/css/mana.min.css';

const app = createApp(App);
const pinia = createPinia();

app.use(pinia);
app.use(PrimeVue);
app.use(ToastService);
app.use(ConfirmationService);
app.use(VueApexCharts);
app.directive('tooltip', Tooltip);
app.directive('badge', BadgeDirective);

app.mount('#app');
// app.config.performance = true;
