import DeckMeta from '@model/types/DeckMeta';
import DeckSlot from '@model/types/DeckSlot';
import DeckStats from '@model/types/DeckStats';
import Deck from '@model/types/Deck';
import StatsHandler from './statsHandler';
import { Board } from '@model/enums';
import MagicCard from '@model/types/MagicCard';
import { StorageFile, StorageValues } from '@model/interfaces/Storage';

export type EditableSlotFields = 'quantity' | 'comment' | 'overrideCmc';

class DeckHandler {
  static createDeckFromPath(path: string): Deck | null {
    let fixedMeta = false;
    let fixedSlots = false;
    let cardCounter = 0;
    let storageData: StorageFile | null = null;

    try {
      storageData = window.modelService.Decks.getData(path);
    } catch (err) {
      console.error(err, path);

      return null;
    }

    // meta
    const meta = DeckMeta.fromStorage(storageData);

    // slots
    const slots = [];
    for (let i = 0; i < storageData.slots.length; i++) {
      const storageSlot = storageData.slots[i];
      const card = findCard(storageSlot.name);

      if (card) {
        const slot = DeckSlot.fromStorage(storageData.slots[i], card);

        // fill missing fields like oracleId and safe on the fly
        if (!storageSlot.oracleId) {
          fixedSlots = true; // any card fixed?
        }

        slots.push(slot);
        cardCounter += slot.quantity;
      }
    }

    // stats
    const stats = new DeckStats();
    StatsHandler.calculate(stats, slots, window.appService.getCurrency());

    const newDeck = new Deck(meta, stats, slots, path);

    // missing data of original json
    if (!storageData.colors) {
      if (fillColors(newDeck)) {
        fixedMeta = true;
      }
    }
    if (!storageData.type) {
      if (cardCounter >= 100) {
        newDeck.meta.type = 'edh';
        newDeck.meta.formats = ['commander'];
        fixedMeta = true;
      }
    }

    // save changes during building
    if (fixedSlots) {
      DeckHandler.pushSlotsUpdate(newDeck);
    }

    if (fixedMeta) {
      DeckHandler.pushMetaUpdate(newDeck);
    }

    return newDeck;
  }

  /*
  static createSlotsFromRegister(register: DeckRegister): DeckSlot[] {
    const deckSlots: DeckSlot[] = [];
    for (const slot of register.slots) {
      const card = window.modelService.Cards.byOracleId(slot.oracleId);
      if (card) {
        deckSlots.push(new DeckSlot(card, slot.quantity, slot.board));
      }
    }

    return deckSlots;
  } */

  static pushSlotsUpdate(deck: Deck): void {
    const path = deck.path;
    const slotArray = Object.values(deck.slots);
    const slots = slotArray.map((slotProxy) => slotProxy.reduce());
    window.modelService.Decks.setField(path, 'slots', slots);
    window.modelService.Decks.sync(path, { slots });
  }

  static pushMetaUpdate(deck: Deck): void {
    const path = deck.path;
    const updatedFields: { [index: string]: StorageValues } = {};
    for (const k in deck.meta) {
      const metaValue = JSON.parse(JSON.stringify(deck.meta[k]));
      if (k !== path) {
        // weired field with path as key
        window.modelService.Decks.setField(path, k, metaValue);
        updatedFields[k] = metaValue as StorageValues;
      }
    }

    window.modelService.Decks.sync(path, updatedFields);
  }

  static addSlot(deck: Deck, card: MagicCard, quantity: number, board: Board): void {
    for (let i = 0; i < deck.slots.length; i++) {
      if (deck.slots[i] && deck.slots[i].oracleId === card.oracleId && deck.slots[i].board === board) {
        // console.log('old', deck.slots[i], board);
        deck.slots[i].quantity += quantity;
        refreshStats(deck);
        return;
      }
    }

    const slot = new DeckSlot(card, quantity, board);
    deck.slots.push(slot);
    refreshStats(deck);
  }

  static removeSlot(deck: Deck, oracleId: string, board: string): void {
    for (let i = 0; i < deck.slots.length; i++) {
      if (deck.slots[i] && deck.slots[i].oracleId === oracleId && deck.slots[i].board === board) {
        // console.log('remove', deck.slots, oracleId, board, deck.slots[i]);
        delete deck.slots[i];
        break;
      }
    }

    // check signature and generals
    if (deck.meta.generals.includes(oracleId)) {
      deck.meta.generals = deck.meta.generals.filter((oid: string) => oid !== oracleId);
    }

    if (deck.meta.signatures.includes(oracleId)) {
      deck.meta.signatures = deck.meta.signatures.filter((oid: string) => oid !== oracleId);
    }

    refreshStats(deck);
  }

  static increaseSlot(deck: Deck, oracleId: string, board: Board): void {
    for (let i = 0; i < deck.slots.length; i++) {
      if (deck.slots[i] && deck.slots[i].oracleId === oracleId && deck.slots[i].board === board) {
        deck.slots[i].quantity++;
        refreshStats(deck);
        return;
      }
    }
  }

  static decreaseSlot(deck: Deck, oracleId: string, board: Board): void {
    for (let i = 0; i < deck.slots.length; i++) {
      if (deck.slots[i] && deck.slots[i].oracleId === oracleId && deck.slots[i].board === board) {
        if (deck.slots[i].quantity > 1) {
          deck.slots[i].quantity--;
          refreshStats(deck);
        } else {
          DeckHandler.removeSlot(deck, oracleId, board);
        }
        return;
      }
    }
  }

  static updateSlotValue(deck: Deck, oracleId: string, board: Board, field: EditableSlotFields, value: string | number): void {
    for (let i = 0; i < deck.slots.length; i++) {
      if (deck.slots[i] && deck.slots[i].oracleId === oracleId && deck.slots[i].board === board) {
        // this does not reindex array numbers, so check for [i] keeps length
        switch (field) {
          case 'overrideCmc':
            deck.slots[i].overrideCmc = value as number;
            break;

          case 'quantity':
            deck.slots[i].quantity = value as number;
            refreshStats(deck);
            break;

          case 'comment':
            deck.slots[i].comment = value as string;
            break;
        }

        return;
      }
    }
  }

  static moveSlotBoard(deck: Deck, oracleId: string, fromBoard: Board, toBoard: Board): void {
    DeckHandler.removeSlot(deck, oracleId, fromBoard);

    const card = window.modelService.Cards.byOracleId(oracleId);
    if (card) {
      DeckHandler.addSlot(deck, card, 1, toBoard);
    }
  }

  static moveSlotBottom(deck: Deck, oracleId: string, board: Board): void {
    for (let i = 0; i < deck.slots.length; i++) {
      if (deck.slots[i] && deck.slots[i].oracleId === oracleId && deck.slots[i].board === board) {
        const slot = deck.slots[i];
        DeckHandler.removeSlot(deck, oracleId, board);
        deck.slots.push(slot);
        refreshStats(deck);
        return;
      }
    }
  }

  /**
   * Sort array by cmc, cost, name
   */
  static sortSlots(slots: DeckSlot[]): void {
    DeckHandler.sortSlotsByName(slots);

    slots.sort((slotA, slotB) => {
      if (slotA.cost > slotB.cost) {
        return 1;
      }
      if (slotA.cost < slotB.cost) {
        return -1;
      }
      return 0;
    });

    slots.sort((slotA, slotB) => {
      return slotA.cmc - slotB.cmc;
    });
  }

  /**
   * Sort array by name only
   */
  static sortSlotsByName(slots: DeckSlot[]): void {
    slots.sort((slotA, slotB) => {
      if (slotA.name > slotB.name) {
        return 1;
      }
      if (slotA.name < slotB.name) {
        return -1;
      }
      return 0;
    });
  }
}

export default DeckHandler;

function refreshStats(deck: Deck): void {
  // TODO: single card update
  StatsHandler.calculate(deck.stats, deck.slots, window.appService.getCurrency());
}

function findCard(name: string): MagicCard | undefined {
  let searchName = name.replace(/Æ/, 'Ae');

  // clear "(Fire)"
  searchName = searchName.replace(/\(.*?\)/, '').trim();

  const card = window.modelService.Cards.byName(searchName);

  if (card) {
    return card;
  }

  // free search maybe?
  const cards = window.modelService.Cards.search(searchName);
  if (cards.length === 1) {
    return cards[0];
  }

  console.error('Failed to find card by oracle id!', card, name);

  return undefined;
}

// try to find deck colors by land types, color identities are not good enough
function fillColors(deck: Deck): boolean {
  try {
    let colors: string[] = [];
    for (const slot of deck.slots) {
      if (slot.linkedCard.subTypes.includes('Swamp')) {
        colors.push('B');
      }
      if (slot.linkedCard.subTypes.includes('Forest')) {
        colors.push('G');
      }
      if (slot.linkedCard.subTypes.includes('Mountain')) {
        colors.push('R');
      }
      if (slot.linkedCard.subTypes.includes('Island')) {
        colors.push('U');
      }
      if (slot.linkedCard.subTypes.includes('Plains')) {
        colors.push('W');
      }
    }
    colors = Array.from(new Set(colors));
    colors.sort();
    deck.meta.colors = colors;

    return true;
  } catch (error) {
    console.warn(error);
  }

  return false;
}
