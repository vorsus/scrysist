import EventHandler from './eventHandler';
import DeckHandler from './deckHandler';
import DeckMeta from '@model/types/DeckMeta';
import DeckSlot from '@model/types/DeckSlot';

class ExportHandler {
  static fromSlots(slots: DeckSlot[], meta: DeckMeta): string {
    try {
      const main: string[] = [];
      const side: string[] = [];
      let companion: string | null = null;

      DeckHandler.sortSlotsByName(slots);

      for (const slot of slots) {
        if (slot) {
          if (slot.board === 'main') {
            main.push(`${slot.quantity} ${slot.name}`);
          } else if (slot.board === 'side') {
            side.push(`${slot.quantity} ${slot.name}`);
          }
        }
      }

      if (meta.companions.length) {
        const card = window.modelService.Cards.byOracleId(meta.companions[0]);
        companion = `1 ${card?.englishName}`;
      }

      let s = '';
      if (companion) {
        s += `Companion\n${companion}\n\n`;
      }

      if (companion || side.length > 0) {
        s += `Deck\n`; // only if headlines needed
      }
      s += main.join('\n') + '\n\n';

      if (side.length > 0) {
        s += `Sideboard\n`;
        s += side.join('\n');
      }

      return s.trim();
    } catch (error) {
      EventHandler.emit('show-warning', error);
    }

    return '';
  }
}

export default ExportHandler;
