/**
 * Thanks to https://github.com/e-oj/Magic-Grid
 */

export type ResponsiveGridConfig = {
  container: HTMLElement;
  gutter: number;
  maxColumns?: number;
  useTransform?: boolean;
  useMin?: boolean;
  center?: boolean;
  animate?: boolean;
  staticSize?: number;
  offsetY?: number;
  offsetX?: number;
};

type GridColumn = {
  height: number;
  index: number;
};

class ResponsiveGrid {
  container: HTMLElement;
  gutter: number;
  styledItems: Set<HTMLElement>;
  animate: boolean;
  useTransform: boolean;
  center: boolean;
  useMin: boolean;
  maxColumns: number;
  staticSize: number;
  offsetY: number;
  offsetX: number;

  constructor(config: ResponsiveGridConfig) {
    this.container = config.container;
    this.staticSize = config.staticSize || 0;
    this.gutter = config.gutter || 25;
    this.maxColumns = config.maxColumns || 5;
    this.useMin = config.useMin || false;
    this.useTransform = config.useTransform || false;
    this.animate = config.animate || false;
    this.center = config.center || false;
    this.styledItems = new Set();
    this.offsetY = config.offsetY || 0;
    this.offsetX = config.offsetX || 0;
  }

  /**
   * Positions each item in the grid, based
   * on their corresponding column's height
   * and index then stretches the container to
   * the height of the grid.
   */
  positionItems(): void {
    // console.log('pos', this.container.getBoundingClientRect());

    const { cols, wSpace, colWidth } = this.setup();
    const items = this.items();
    const offsetSpace = this.center ? Math.floor(wSpace / 2) : 0;
    let maxHeight = 0;

    this.initStyles();

    for (let i = 0; i < items.length; i++) {
      const item = items[i];

      const col = this.nextCol(cols, i);
      const topGutter = col.height ? this.gutter : 0;
      const left = col.index * colWidth + offsetSpace + this.offsetX + 'px';
      const top = col.height + topGutter + this.offsetY + 'px';

      if (this.useTransform) {
        item.style.transform = `translate(${left}, ${top})`;
      } else {
        item.style.top = top;
        item.style.left = left;
      }

      col.height += item.getBoundingClientRect().height + topGutter;

      if (col.height > maxHeight) {
        maxHeight = col.height;
      }
    }

    // console.log(this.container.style.height);
    this.container.style.height = maxHeight + this.gutter + 'px';
  }

  private initStyles(): void {
    this.container.style.position = 'relative';
    const items = this.items();

    for (let i = 0; i < items.length; i++) {
      if (this.styledItems.has(items[i])) {
        continue;
      }

      items[i].style.position = 'absolute';

      if (this.animate) {
        items[i].style.transition = `${this.useTransform ? 'transform' : 'top, left'} 0.2s ease`;
      }

      // console.log('initStyles', items[i]);
      this.styledItems.add(items[i]);
    }
  }

  private items(): HTMLElement[] {
    const elements: HTMLElement[] = [];
    for (let i = 0; i < this.container.children.length; i++) {
      elements.push(this.container.children[i] as HTMLElement);
    }

    return elements;
  }

  /**
   * Calculates the width of a column.
   */
  private colWidth(): number {
    return this.items()[0].getBoundingClientRect().width + this.gutter;
  }

  /**
   * Initializes an array of empty columns
   * and calculates the leftover whitespace.
   */
  private setup(): { cols: GridColumn[]; wSpace: number; colWidth: number } {
    const width = this.container.getBoundingClientRect().width;
    const colWidth = this.colWidth();
    const cols = [];
    let numCols = Math.floor(width / colWidth) || 1;

    if (this.maxColumns && numCols > this.maxColumns) {
      numCols = this.maxColumns;
    }

    for (let i = 0; i < numCols; i++) {
      cols[i] = { height: 0, index: i };
    }

    const wSpace = width - numCols * colWidth + this.gutter;

    return { cols, wSpace, colWidth };
  }

  /**
   * Gets the next available column.
   */
  private nextCol(cols: GridColumn[], i: number): GridColumn {
    if (this.useMin) {
      return getMin(cols);
    }

    return cols[i % cols.length];
  }
}

export default ResponsiveGrid;

function getMin(cols: GridColumn[]): GridColumn {
  let min = cols[0];

  for (const col of cols) {
    if (col.height < min.height) {
      min = col;
    }
  }

  return min;
}
