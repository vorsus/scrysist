import { COMPANIONS, CardType, DECK_BOARD_SIZES, DECK_TYPES, isColorIdentityRequired } from '@model/enums';
import Deck from '@model/types/Deck';
import MagicCard from '@model/types/MagicCard';
import DeckSlot from '@model/types/DeckSlot';
import ColorHandler from './colorHandler';

class CompanionHandler {
  static calculateForDeck(deck: Deck, card: MagicCard): number | undefined {
    const mainSlots = deck.slots.filter((slot) => slot.board === 'main');
    const nonlandSlots = mainSlots.filter((slot) => false === slot.linkedCard.isLand);

    if (isColorIdentityRequired(deck.meta.type) && !ColorHandler.compare(deck.meta.colors, card.colorIdentity, 'includes')) {
      return 0;
    }

    switch (card.id) {
      case COMPANIONS.Lutri:
        if (deck.meta.type === DECK_TYPES.EDH) {
          return 0; // pretty sure this is banned forever...
        }
        return checkLutri(nonlandSlots);
      case COMPANIONS.Lurrus:
        return checkLurrus(deck);
      case COMPANIONS.Obosh:
        return checkObosh(nonlandSlots);
      case COMPANIONS.Keruga:
        return checkKeruga(nonlandSlots);
      case COMPANIONS.Umori:
        return checkUmori(deck);
      case COMPANIONS.Zirda:
        return checkZirda(mainSlots);
      case COMPANIONS.Gyruda:
        return checkGyruda(nonlandSlots);
      case COMPANIONS.Jegantha:
        return checkJegantha(nonlandSlots);
      case COMPANIONS.Kaheera:
        return checkKaheera(nonlandSlots);
      case COMPANIONS.Yorion:
        return checkYorion(deck, mainSlots);
    }

    return undefined;
  }
}

export default CompanionHandler;

function checkLutri(nonlandSlots: DeckSlot[]): number {
  const invalidSlots = nonlandSlots.filter((slot) => slot.quantity > 1);

  if (nonlandSlots.length === 0) {
    return 0;
  }

  return Math.ceil(100 - 100 * (invalidSlots.length / nonlandSlots.length));
}

function checkLurrus(deck: Deck): number {
  // non spells > 2 cmc
  const totalInvalid =
    deck.stats.cmc[3].nc +
    deck.stats.cmc[3].c +
    deck.stats.cmc[4].nc +
    deck.stats.cmc[4].c +
    deck.stats.cmc[5].nc +
    deck.stats.cmc[5].c +
    deck.stats.cmc[6].nc +
    deck.stats.cmc[6].c +
    deck.stats.cmc[7].nc +
    deck.stats.cmc[7].c;

  let totalCards = 0;
  let ty: CardType;
  for (ty in deck.stats.total) {
    if (ty !== 'lands') {
      totalCards += deck.stats.total[ty];
    }
  }

  if (totalCards === 0) {
    return 0;
  }

  if (totalInvalid === 0) {
    return 100;
  }

  return Math.ceil(100 - 100 * (totalInvalid / totalCards));
}

function checkKeruga(nonlandSlots: DeckSlot[]): number {
  const totalCards = nonlandSlots.length;
  if (totalCards === 0) {
    return 0;
  }

  const totalInvalid = nonlandSlots.filter((slot) => slot.linkedCard.cmc < 3).length;
  if (totalInvalid === 0) {
    return 100;
  }

  return Math.ceil(100 - 100 * (totalInvalid / totalCards));
}

function checkUmori(deck: Deck): number {
  let maxTotal = 0;
  let totalCards = 0;
  let ty: CardType;
  for (ty in deck.stats.total) {
    if (ty !== 'lands') {
      maxTotal = Math.max(maxTotal, deck.stats.total[ty]);
      totalCards += deck.stats.total[ty];
    }
  }

  return Math.ceil(100 * (maxTotal / totalCards));
}

function checkObosh(nonlandSlots: DeckSlot[]): number {
  const totalCards = nonlandSlots.length;

  if (totalCards === 0) {
    return 0;
  }

  const totalInvalid = nonlandSlots.filter((slot) => slot.linkedCard.cmc % 2 === 0).length;

  if (totalInvalid === 0) {
    return 100;
  }

  return Math.ceil(100 - 100 * (totalInvalid / totalCards));
}

function checkGyruda(nonlandSlots: DeckSlot[]): number {
  const totalCards = nonlandSlots.length;
  if (totalCards === 0) {
    return 0;
  }

  const totalInvalid = nonlandSlots.filter((slot) => slot.linkedCard.cmc % 2 === 1).length;
  if (totalInvalid === 0) {
    return 100;
  }

  return Math.ceil(100 - 100 * (totalInvalid / totalCards));
}

function checkZirda(mainSlots: DeckSlot[]): number {
  const totalCards = mainSlots.length;
  if (totalCards === 0) {
    return 0;
  }

  // this may be too simple
  const totalInvalid = mainSlots.filter((slot) => slot.linkedCard.isPermanent && !slot.linkedCard.oracleTexts[0].includes(':')).length;

  if (totalInvalid === 0) {
    return 100;
  }

  return Math.ceil(100 - 100 * (totalInvalid / totalCards));
}

function checkKaheera(nonlandSlots: DeckSlot[]): number {
  const totalCards = nonlandSlots.length;
  if (totalCards === 0) {
    return 0;
  }

  const totalInvalid = nonlandSlots.filter(
    (slot) =>
      slot.linkedCard.isCreature &&
      !slot.linkedCard.subTypes.includes('Beast') &&
      !slot.linkedCard.subTypes.includes('Cat') &&
      !slot.linkedCard.subTypes.includes('Elemental') &&
      !slot.linkedCard.subTypes.includes('Nightmare') &&
      !slot.linkedCard.subTypes.includes('Dinosaur') &&
      !slot.linkedCard.keywords.includes('Changeling')
  ).length;

  return Math.ceil(100 - 100 * (totalInvalid / totalCards));
}

function checkJegantha(nonlandSlots: DeckSlot[]): number {
  const totalCards = nonlandSlots.length;
  if (totalCards === 0) {
    return 0;
  }

  let totalInvalid = 0;
  nonlandSlots.forEach((slot) => {
    const costCleanArray = (slot.cost.match(/{.+?}/g) || []).sort();
    let prev: string | null = null;

    costCleanArray.every((item) => {
      if (prev === item) {
        totalInvalid++;
        return false;
      }

      prev = item;
      return true;
    });
  });

  return Math.ceil(100 - 100 * (totalInvalid / totalCards));
}

function checkYorion(deck: Deck, mainSlots: DeckSlot[]): number {
  try {
    const sizeTargets = DECK_BOARD_SIZES[deck.meta.type];
    const minQuantity = sizeTargets[0] + 20;

    // exceeded max size (e.g. EDH)
    if (minQuantity > sizeTargets[1]) {
      return 0;
    }

    let totalQuantity = 0;
    mainSlots.forEach((slot) => {
      totalQuantity += slot.quantity;
    });

    return totalQuantity >= minQuantity ? 100 : Math.ceil(100 * (totalQuantity / minQuantity));
  } catch (err) {
    return 0;
  }
}
