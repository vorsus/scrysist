const convertBytes = (bytes: number, decimals = 2): string => {
  const units = ['B', 'KB', 'MB', 'GB', 'TB'];

  let i = 0;
  for (i; bytes > 1024; i++) {
    bytes /= 1024;
  }

  return parseFloat(bytes.toFixed(decimals)) + ' ' + units[i];
};

const extractTitle = (path: string): string => {
  const pos = path.lastIndexOf(window.appService.pathSeparator());

  if (pos !== -1) {
    path = path.substring(pos + 1);
  }

  return path.replace(/.json/, '');
};

export { convertBytes, extractTitle };
