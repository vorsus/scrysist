import { autoParse } from 'mtg-decklist-parser2';
import EventHandler from './eventHandler';
import MagicCard from '@model/types/MagicCard';

interface CardAmount {
  card: MagicCard;
  amount?: number;
}

export type ParseResult = {
  successMain: CardAmount[];
  missedMain: string[];
  successSide: CardAmount[];
  missedSide: string[];
  companion: CardAmount | null;
} | null;

class ImportHandler {
  static parseText(value: string): ParseResult {
    const successMain: CardAmount[] = [];
    const missedMain = [];
    const successSide: CardAmount[] = [];
    const missedSide = [];
    let companion: CardAmount | null = null;

    try {
      // console.info(value);
      const dl = autoParse(value);
      if (dl.valid) {
        for (const cm of dl.deck) {
          const card = findCardByName(cm.name);
          if (card) {
            successMain.push({ card, amount: cm.amount });
          } else {
            missedMain.push(cm.toCardString());
          }
        }

        for (const cm of dl.sideboard) {
          const card = findCardByName(cm.name);
          if (card) {
            successSide.push({ card, amount: cm.amount });
          } else {
            missedSide.push(cm.toCardString());
          }
        }

        if (dl.companion) {
          const card = findCardByName(dl.companion.name);
          if (card) {
            companion = { card };
          }
        }
      } else {
        throw new Error('Invalid format');
      }
    } catch (error) {
      EventHandler.emit('show-warning', error);

      return null;
    }

    return { successMain, missedMain, successSide, missedSide, companion };
  }
}

export default ImportHandler;

function findCardByName(name: string): MagicCard | null {
  // console.log(name, window.modelService.Cards.byName(name), window.modelService.Cards.search(name));

  const card = window.modelService.Cards.byName(name);
  if (card) {
    return card;
  }

  const cards = window.modelService.Cards.search(name);
  if (cards.length === 1) {
    return cards[0];
  }

  return null;
}
