import DeckStats from '@model/types/DeckStats';
import MagicTag, { formatTag } from '@model/types/MagicTag';
import { COLOR_NAMES, TAG_TYPES } from '@model/enums';
import MagicCard from '@model/types/MagicCard';
import DeckSlot from '@model/types/DeckSlot';

const cardTypeThreshold = 20;
const keywordThreshold = 15;
const tribalThreshold = 10;
const defaultColorThreshold = 25;
const monoColorThreshold = 85;

const tribalBlackList = ['swamp', 'mountain', 'plains', 'island', 'forest'];
const cardTypeBlackList = ['land', 'basic'];

type LightSlot = { linkedCard: MagicCard; quantity: number };

class TagsHandler {
  static filterForDeck<T extends MagicTag>(tags: T[], filterCallback: (tag: T) => void, stats: DeckStats, slots: DeckSlot[]): T[] {
    return tags.filter((tag) => {
      let keep = true;
      switch (tag.type) {
        case TAG_TYPES.KEYWORD:
          keep = checkKeywords(tag, slots, stats.getTotal());
          break;
        case TAG_TYPES.TRIBAL:
          keep = checkTribal(tag, slots, stats.getTotal());
          break;
        case TAG_TYPES.CARD_TYPE:
          keep = checkCardType(tag, slots, stats.getTotal());
          break;
        case TAG_TYPES.COLOR:
          keep = checkColor(tag, slots, false);
          break;
      }

      if (!keep) {
        filterCallback(tag);
      }

      return keep;
    });
  }

  static filterForCard(tags: MagicTag[], card: MagicCard): MagicTag[] {
    return tags.filter((tag) => {
      let keep = false;
      switch (tag.type) {
        case TAG_TYPES.KEYWORD:
          keep = checkKeywords(tag, [{ quantity: 1, linkedCard: card }], 1);
          break;
        case TAG_TYPES.TRIBAL:
          keep = checkTribal(tag, [{ quantity: 1, linkedCard: card }], 1);
          break;
        case TAG_TYPES.CARD_TYPE:
          keep = checkCardType(tag, [{ quantity: 1, linkedCard: card }], 1);
          break;
        case TAG_TYPES.COLOR:
          keep = checkColor(tag, [{ quantity: 1, linkedCard: card }], true);
          break;
      }

      return keep;
    });
  }

  static createCustomTag(name: string): MagicTag {
    return new MagicTag(formatTag(name), TAG_TYPES.CUSTOM);
  }
}

export default TagsHandler;

function checkCardType(tag: MagicTag, slots: LightSlot[], totalCards: number): boolean {
  let slotCards = 0;

  if (cardTypeBlackList.includes(tag.name)) {
    return false;
  }

  for (const slot of slots) {
    if (slot.linkedCard.superTypes.includes(tag.name.toLowerCase())) {
      slotCards += slot.quantity;
    }
  }

  if (slotCards > 0) {
    return (slotCards / totalCards) * 100 >= cardTypeThreshold;
  }

  return false;
}

function checkKeywords(tag: MagicTag, slots: LightSlot[], totalCards: number): boolean {
  let slotCards = 0;

  for (const slot of slots) {
    if (slot.linkedCard.keywords.includes(formatTag(tag.name))) {
      slotCards += slot.quantity;
    }
  }

  if (slotCards > 0) {
    return (slotCards / totalCards) * 100 >= keywordThreshold;
  }

  return false;
}

function checkTribal(tag: MagicTag, slots: LightSlot[], totalCards: number): boolean {
  let slotCards = 0;

  if (tribalBlackList.includes(tag.name.toLowerCase())) {
    return false;
  }

  for (const slot of slots) {
    if (slot.linkedCard.subTypes.includes(formatTag(tag.name))) {
      slotCards += slot.quantity;
    }
  }

  if (slotCards > 0) {
    return (slotCards / totalCards) * 100 >= tribalThreshold;
  }

  return false;
}

function checkColor(tag: MagicTag, slots: LightSlot[], ignoreMono: boolean): boolean {
  let slotCards = 0;
  let totalCards = 0;

  // find the colors for the tag name (uw, brw, etc.)
  let mappedColors: string | undefined = undefined;
  for (const c in COLOR_NAMES) {
    if (COLOR_NAMES[c] === tag.name) {
      mappedColors = c;
      break;
    }
  }

  // something went wrong
  if (!mappedColors) {
    console.warn('Invalid color tag, no corresponding mapping found!');
    return false;
  }

  if (ignoreMono && mappedColors.length <= 1) {
    return false;
  }

  let overallColors: string[] = [];
  for (const slot of slots) {
    if (slot.linkedCard.colors.length > 0) {
      overallColors = overallColors.concat(slot.linkedCard.colors);

      if (slot.linkedCard.colors.join('') === mappedColors) {
        // console.log('Found', mappedColors, slot.linkedCard.englishName, slot.linkedCard.colors);
        slotCards += slot.quantity;
      }

      totalCards += slot.quantity;
    }
  }

  if (overallColors.length > 0) {
    overallColors = Array.from(new Set(overallColors));
    overallColors.sort();

    if (mappedColors.length >= 3 && overallColors.join('').indexOf(mappedColors) !== -1) {
      return true;
    }

    const colorThreshold = mappedColors.length === 1 ? monoColorThreshold : defaultColorThreshold;

    if (slotCards > 0) {
      return (slotCards / totalCards) * 100 >= colorThreshold;
    }
  }

  return false;
}
