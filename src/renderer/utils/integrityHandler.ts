import { isBasicLand, DECK_BOARD_SIZES } from '@model/enums';
import Deck from '@model/types/Deck';

class IntegrityHandler {
  static check = (deck: Deck): string[] => {
    try {
      const issues: string[] = [];
      const targetSizes = DECK_BOARD_SIZES[deck.meta.type];
      let totalMain = 0;
      let totalSide = 0;

      for (const slot of deck.slots) {
        // after remove it may be undefined
        if (slot) {
          if (slot.board === 'consider') {
            continue;
          }

          const q = slot.quantity;
          if (slot.board === 'side') {
            totalSide += q;
          } else {
            totalMain += q;
          }

          const card = slot.linkedCard;

          // TODO: check main and side combined
          if (q > targetSizes[3] && !isBasicLand(card.englishName) && card.oracleTexts[0].indexOf('A deck can have any number of cards named') === -1) {
            issues.push('Exceeded max allowed quantity: ' + card.englishName);
          }
        }
      }

      if (totalMain < targetSizes[0] || totalMain > targetSizes[1] || totalSide > targetSizes[2]) {
        issues.push('Deck size not legal (main: ' + totalMain + ', side: ' + totalSide + ')');
      }

      // TODO: check color identity
      // TODO: check if general is set and all generals combined do match color identity

      return issues;
    } catch (err) {
      console.error(err);
      return ['Could not parse deck: ' + deck.path];
    }
  };
}

export default IntegrityHandler;
