import DeckStats from '@model/types/DeckStats';
import MagicCard from '@model/types/MagicCard';
import { Currency, isBasicLand } from '@model/enums';
import DeckSlot from '@model/types/DeckSlot';

class StatsHandler {
  static calculate = (stats: DeckStats, slots: DeckSlot[], currency: Currency): void => {
    const emptyStats = new DeckStats();

    // reset all values first
    stats.colors = emptyStats.colors;
    stats.manaProduction = emptyStats.manaProduction;
    stats.colorIdentity = emptyStats.colorIdentity;
    stats.types = emptyStats.types;
    stats.total = emptyStats.total;
    stats.cmc = emptyStats.cmc;
    stats.price = Number(0);
    stats.avgEdhrecRank = Number(0);

    let edhrecRankSum = 0;
    let edhrecRankCount = 0;

    for (const slot of slots) {
      // after remove it is undefined
      if (slot) {
        const card = slot.linkedCard;
        const q = slot.quantity;
        const isBasicLandCard = isBasicLand(card.englishName);

        if (currency !== 'none' && slot.board !== 'consider' && currency in card.prices && !isBasicLandCard) {
          stats.price += Number(card.prices[currency]) * q;
        }

        if (slot.board === 'main') {
          for (let m = 0; m < card.producedMana.length; m++) {
            stats.manaProduction[card.producedMana[m]] += q;
          }

          for (let m = 0; m < card.colorIdentity.length; m++) {
            stats.colorIdentity[card.colorIdentity[m]] += q;
          }

          if (!isBasicLandCard && card.edhrecRank) {
            edhrecRankSum += card.edhrecRank;
            edhrecRankCount++;
          }

          // this has priority: artifact/enchantment creature -> creature, enchantment land -> enchantment, enchantment artifact -> enchantment ...
          // skip legendary, basic and tribal

          if (slot.cardGroup === 'creatures') {
            card.subTypes.forEach((type: string) => {
              stats.types.creatures[type] = type in stats.types.creatures ? stats.types.creatures[type] + q : q;
            });
            stats.total.creatures += q;
            addCmc(stats, card, 'c', q);
          } else if (slot.cardGroup === 'instants') {
            card.subTypes.forEach((type: string) => {
              stats.types.instants[type] = type in stats.types.instants ? stats.types.instants[type] + q : q;
            });
            stats.total.instants += q;
            addCmc(stats, card, 's', q);
          } else if (slot.cardGroup === 'sorceries') {
            card.subTypes.forEach((type: string) => {
              stats.types.sorceries[type] = type in stats.types.sorceries ? stats.types.sorceries[type] + q : q;
            });
            stats.total.sorceries += q;
            addCmc(stats, card, 's', q);
          } else if (slot.cardGroup === 'planeswalkers') {
            card.subTypes.forEach((type: string) => {
              stats.types.planeswalkers[type] = type in stats.types.planeswalkers ? stats.types.planeswalkers[type] + q : q;
            });
            stats.total.planeswalkers += q;
            addCmc(stats, card, 'nc', q);
          } else if (slot.cardGroup === 'enchantments') {
            card.subTypes.forEach((type: string) => {
              stats.types.enchantments[type] = type in stats.types.enchantments ? stats.types.enchantments[type] + q : q;
            });
            stats.total.enchantments += q;
            addCmc(stats, card, 'nc', q);
          } else if (slot.cardGroup === 'lands') {
            // check land before artifact to avoid artifact lands not counted
            card.subTypes.forEach((type: string) => {
              stats.types.lands[type] = type in stats.types.lands ? stats.types.lands[type] + q : q;
            });
            stats.total.lands += q;

            continue; // no colors
          } else if (slot.cardGroup === 'artifacts') {
            card.subTypes.forEach((type: string) => {
              stats.types.artifacts[type] = type in stats.types.artifacts ? stats.types.artifacts[type] + q : q;
            });
            stats.total.artifacts += q;
            addCmc(stats, card, 'nc', q);
          } else if (slot.cardGroup === 'battles') {
            card.subTypes.forEach((type: string) => {
              stats.types.battles[type] = type in stats.types.battles ? stats.types.battles[type] + q : q;
            });
            stats.total.battles += q;
            addCmc(stats, card, 'nc', q);
          } else if (slot.cardGroup === 'others') {
            stats.total.others += q;
          }

          const colors = card.colors;
          colors.sort();

          if (colors.length === 0) {
            stats.colors.C += q;
          } else if (colors.length === 1) {
            stats.colors[colors[0]] += q;
          } else if (colors.length === 4) {
            stats.colors.C4 += q;
          } else if (colors.length === 5) {
            stats.colors.C5 += q;
          } else {
            stats.colors[colors.join('')] += q;
          }
        }
      }
    }

    stats.avgEdhrecRank = edhrecRankCount > 0 ? Math.ceil(edhrecRankSum / edhrecRankCount) : 0;
  };
}

export default StatsHandler;

function addCmc(stats: DeckStats, card: MagicCard, where: 'c' | 'nc' | 's', quantity: number): void {
  stats.cmc[Math.min(card.cmc, 7)][where] += quantity;
}
