import { ActualColors, COLOR_NAMES, PLAIN_COLORS } from '@model/enums';
import { StrStr } from '@model/interfaces/KeyPairs';

/*
const documentStyle = getComputedStyle(document.documentElement);

const colorRed = documentStyle.getPropertyValue('--mana-red').trim();
const colorGreen = documentStyle.getPropertyValue('--mana-green').trim();
const colorBlue = documentStyle.getPropertyValue('--mana-blue').trim();
const colorBlack = documentStyle.getPropertyValue('--mana-black').trim();
const colorWhite = documentStyle.getPropertyValue('--mana-white').trim();
const colorLess = documentStyle.getPropertyValue('--mana-colorless').trim();
const multicolor = documentStyle.getPropertyValue('--mana-multicolor').trim();
const typeSpell = documentStyle.getPropertyValue('--type-spell').trim();
const typeNonCreature = documentStyle.getPropertyValue('--type-non-creature').trim();
const typeCreature = documentStyle.getPropertyValue('--type-creature').trim();
const typeLand = documentStyle.getPropertyValue('--type-land').trim();
*/
const colorRed = '#e24a0e';
const colorGreen = '#4bc911';
const colorBlue = '#2a8aeb';
const colorBlack = '#0a0b0c';
const colorWhite = '#e6e6e6';
const colorLess = '#a1a1a1';
const multicolor = '#e6ad13';
const typeSpell = '#e495ee';
const typeNonCreature = '#10d3d3';
const typeCreature = '#e2d40a';
const typeLand = '#c2c4c2';

const mapSingle: StrStr = {
  B: colorBlack,
  G: colorGreen,
  R: colorRed,
  U: colorBlue,
  W: colorWhite,
  C: colorLess
};

const colorHandler = {
  TypeCreature: typeCreature,
  TypeNonCreature: typeNonCreature,
  TypeLand: typeLand,
  TypeSpell: typeSpell,

  Red: colorRed,
  Blue: colorBlue,
  Green: colorGreen,
  White: colorWhite,
  Black: colorBlack,
  Colorless: colorLess,
  Multicolor: multicolor,

  mapSingle,

  mapNames: {
    B: PLAIN_COLORS.B,
    G: PLAIN_COLORS.G,
    R: PLAIN_COLORS.R,
    U: PLAIN_COLORS.U,
    W: PLAIN_COLORS.W,

    C: COLOR_NAMES.C,

    BG: COLOR_NAMES.BG,
    BR: COLOR_NAMES.BR,
    BU: COLOR_NAMES.BU,
    BW: COLOR_NAMES.BW,
    GR: COLOR_NAMES.GR,
    GU: COLOR_NAMES.GU,
    GW: COLOR_NAMES.GW,
    RU: COLOR_NAMES.RU,
    RW: COLOR_NAMES.RW,
    UW: COLOR_NAMES.UW,

    BGR: COLOR_NAMES.BGR,
    BGU: COLOR_NAMES.BGU,
    BGW: COLOR_NAMES.BGW,
    BRU: COLOR_NAMES.BRU,
    BRW: COLOR_NAMES.BRW,
    BUW: COLOR_NAMES.BUW,
    GRU: COLOR_NAMES.GRU,
    GRW: COLOR_NAMES.GRW,
    GUW: COLOR_NAMES.GUW,
    RUW: COLOR_NAMES.RUW,

    C4: '4-Color',
    C5: '5-Color'
  } as StrStr,

  getLegend: (el: string): string => {
    if (el in mapSingle) {
      return mapSingle[el];
    }

    return multicolor;
  },

  compare: (colorsA: string[] | ActualColors[], colorsB: string[] | ActualColors[], mode: 'includes' | 'equal'): boolean => {
    if (mode === 'equal') {
      colorsA = colorsA.sort();
      colorsB = colorsB.sort();

      return colorsA.join('') === colorsB.join('');
    }

    return colorsB.every((c: string) => colorsA.includes(c));
  }
};

export default colorHandler;
