const splitSymbols = (cost: string): string[] => {
  const len = cost.length;
  let j = -1;
  let inside = false;
  const result: string[] = [];

  for (let i = 0; i < len; i++) {
    const c = cost[i];
    if ('{' === c) {
      j++;
      inside = true;
      result[j] = c;
    } else if (inside) {
      result[j] += c;
      if ('}' === c) {
        inside = false;
      }
    }
  }

  result.sort();

  return result;
};

const parseTextSymbols = (s: string, size: number): string => {
  const symbolList = window.modelService.Symbols.getList();

  for (const sym of symbolList) {
    s = s.replaceAll(sym.code, '<img src="' + sym.cip + '" height="' + size + '" alt="' + sym.code + '" title="' + sym.name + '">');
  }

  return s;
};

const joinSymbols = (list: string[]): string => {
  list.sort();
  let s = '';
  for (let i = 0; i < list.length; i++) {
    s += '{' + list[i] + '}';
  }

  return s;
};

export { splitSymbols, parseTextSymbols, joinSymbols };
