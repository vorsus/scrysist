import { ProtocolRequest } from 'electron';
import { Response } from 'electron-fetch';
import { writeFile, existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import { promisify } from 'util';

const fetch = require('electron-fetch').default;
const writeFilePromise = promisify(writeFile);

type DownloadCallbackArguments = { path: string | null };
export type DownloadCallback = (x: DownloadCallbackArguments) => void;

const downloadFile = async (url: string, outputPath: string): Promise<void> => {
  // TODO: check for 404
  return fetch(url)
    .then((x: Response) => x.arrayBuffer())
    .then((x: ArrayBuffer) => writeFilePromise(outputPath, Buffer.from(x)));
};

const loadCardNormal = async (res: string, basePath: string, callback: DownloadCallback): Promise<void> => {
  const parts = res.split('@');
  const oracleId = parts[0];
  const remoteUrl = parts[1];

  // TODO: maybe option for highres?
  // TODO: check if image not "missing" ?
  const sub1 = oracleId.substring(0, 2);
  const sub2 = oracleId.substring(2, 4);
  const folderPath = join(basePath, 'cache', 'images', 'normal', sub1, sub2);
  const path = join(folderPath, oracleId + '.jpg');
  // console.log('load normal', remoteUrl, path, parts, basePath);

  try {
    if (!existsSync(path)) {
      if (!existsSync(folderPath)) {
        mkdirSync(folderPath, { recursive: true });
      }

      await downloadFile(remoteUrl, path);
    }

    callback({ path });
  } catch (error) {
    console.warn(error);
  }
};

const loadCardNormalBack = async (res: string, basePath: string, callback: DownloadCallback): Promise<void> => {
  const parts = res.split('@');
  const oracleId = parts[0];
  const remoteUrl = parts[1];

  const sub1 = oracleId.substring(0, 2);
  const sub2 = oracleId.substring(2, 4);
  const folderPath = join(basePath, 'cache', 'images', 'normal_back', sub1, sub2);
  const path = join(folderPath, oracleId + '.jpg');
  // console.log('load normal back', remoteUrl, path, parts, basePath);

  try {
    if (!existsSync(path)) {
      if (!existsSync(folderPath)) {
        mkdirSync(folderPath, { recursive: true });
      }

      await downloadFile(remoteUrl, path);
    }

    callback({ path });
  } catch (error) {
    console.warn(error);
  }
};

const loadCardArt = async (res: string, basePath: string, callback: DownloadCallback): Promise<void> => {
  const parts = res.split('@');
  const oracleId = parts[0];
  const remoteUrl = parts[1];

  const sub1 = oracleId.substring(0, 2);
  const sub2 = oracleId.substring(2, 4);
  const folderPath = join(basePath, 'cache', 'images', 'art', sub1, sub2);
  const path = join(folderPath, oracleId + '.jpg');
  // console.log('load art', remoteUrl, path, parts, basePath);

  try {
    if (!existsSync(path)) {
      if (!existsSync(folderPath)) {
        mkdirSync(folderPath, { recursive: true });
      }

      await downloadFile(remoteUrl, path);
    }

    callback({ path });
  } catch (error) {
    console.warn(error);
  }
};

const loadEditionSvg = async (res: string, basePath: string, callback: DownloadCallback): Promise<void> => {
  const parts = res.split('@');
  const setId = parts[0];
  const remoteUrl = parts[1];

  try {
    const cachePath = join(basePath, 'cache', 'images', 'editions');
    if (!existsSync(cachePath)) {
      mkdirSync(cachePath, { recursive: true });
    }

    // needs prefix, else "con.svg" crashes windows
    const path = join(cachePath, 'set-' + setId + '.svg');
    if (!existsSync(path)) {
      await downloadFile(remoteUrl, path);
    }

    callback({ path });
  } catch (error) {
    console.warn(error);
  }
};

const loadSymbolSvg = async (res: string, basePath: string, callback: DownloadCallback): Promise<void> => {
  const parts = res.split('@');
  const symbolId = parts[0];
  const remoteUrl = parts[1];

  try {
    const cachePath = join(basePath, 'cache', 'images', 'symbols');
    if (!existsSync(cachePath)) {
      mkdirSync(cachePath, { recursive: true });
    }

    const path = join(cachePath, symbolId + '.svg');
    // console.log('load symbol svg', remoteUrl, path, parts, basePath);

    if (!existsSync(path)) {
      await downloadFile(remoteUrl, path);
    }

    callback({ path });
  } catch (error) {
    console.warn(error);
  }
};

export const fetchProxyAsync = (request: ProtocolRequest, basePath: string, callback: DownloadCallback): void => {
  let url = request.url;
  url = url.slice('cip://'.length);
  // console.log(request, url);

  if (url.startsWith('symbol-svg/')) {
    loadSymbolSvg(url.slice('symbol-svg/'.length), basePath, callback);
    return;
  }

  if (url.startsWith('card-normal/')) {
    loadCardNormal(url.slice('card-normal/'.length), basePath, callback);
    return;
  }

  if (url.startsWith('card-normal-back/')) {
    loadCardNormalBack(url.slice('card-normal-back/'.length), basePath, callback);
    return;
  }

  if (url.startsWith('edition-svg/')) {
    loadEditionSvg(url.slice('edition-svg/'.length), basePath, callback);
    return;
  }

  if (url.startsWith('card-art/')) {
    loadCardArt(url.slice('card-art/'.length), basePath, callback);
    return;
  }

  callback({ path: null });
};
