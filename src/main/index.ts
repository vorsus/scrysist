import { app, shell, BrowserWindow, protocol, dialog, ipcMain } from 'electron';
import { join, normalize } from 'path';
import { electronApp, optimizer, is } from '@electron-toolkit/utils';
import { DownloadCallback, fetchProxyAsync } from './imageProxy';
import { initConfig } from './appConfig';
import icon from '../../resources/icon.png?asset';
import { Currency } from '@model/enums';

const appConfig = initConfig(app);

protocol.registerSchemesAsPrivileged([
  { scheme: 'cip', privileges: { bypassCSP: true } },
  { scheme: 'https', privileges: { bypassCSP: true } }
]);

function createWindow(): void {
  let updateConfigJob: NodeJS.Timeout | null = null;

  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: appConfig.get('width') as number,
    height: appConfig.get('height') as number,
    show: false,
    autoHideMenuBar: true,
    ...(process.platform === 'linux' ? { icon } : {}),
    webPreferences: {
      preload: normalize(join(__dirname, '..', 'preload', 'index.js')),
      sandbox: false,
      additionalArguments: [
        // does not work with multiple nor spaces
        '--options=' +
          JSON.stringify({
            basePath: appConfig.get('basePath'),
            openLimit: appConfig.get('openLimit'),
            currency: appConfig.get('currency')
          })
      ]
    }
  });

  mainWindow.on('ready-to-show', () => {
    mainWindow.show();
  });

  const updateConfig = (): void => {
    console.log('Update app config: ', mainWindow.getSize());

    const sizes = mainWindow.getSize();
    appConfig.set('width', sizes[0]);
    appConfig.set('height', sizes[1]);
  };

  // https://www.electronjs.org/docs/latest/api/app
  mainWindow.on('resize', () => {
    // linux does not support "resized" event, so each pixel is tracked
    // be sure to fire this only once at the end
    if (updateConfigJob) {
      clearTimeout(updateConfigJob);
    }

    updateConfigJob = setTimeout(updateConfig, 2000);
  });

  mainWindow.webContents.setWindowOpenHandler((details) => {
    // console.log(details.url);
    shell.openExternal(details.url);
    return { action: 'deny' };
  });

  ipcMain.handle('main-open-console', () => {
    mainWindow.webContents.openDevTools();
  });

  // HMR for renderer base on electron-vite cli.
  // Load the remote URL for development or the local html file for production.
  if (is.dev && process.env['ELECTRON_RENDERER_URL']) {
    mainWindow.loadURL(process.env['ELECTRON_RENDERER_URL']);
  } else {
    mainWindow.loadFile(normalize(join(__dirname, '..', 'renderer', 'index.html')));
  }
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  // Set app user model id for windows
  electronApp.setAppUserModelId('com.electron');

  // Default open or close DevTools by F12 in development
  // and ignore CommandOrControl + R in production.
  // see https://github.com/alex8088/electron-toolkit/tree/master/packages/utils
  app.on('browser-window-created', (_, window) => {
    optimizer.watchWindowShortcuts(window);
  });

  // see https://www.electronjs.org/docs/latest/api/protocol
  protocol.registerFileProtocol('cip', (request, callback) => {
    fetchProxyAsync(request, appConfig.get('basePath') as string, callback as DownloadCallback);
  });

  createWindow();

  app.on('activate', function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

// some handlers

ipcMain.handle('main-relaunch', () => {
  app.relaunch();
  app.exit(0);
});

ipcMain.handle('main-update-settings', (_ev, basePath: string, openLimit: number, currency: Currency) => {
  appConfig.set('basePath', basePath);
  appConfig.set('openLimit', openLimit);
  appConfig.set('currency', currency);
});

ipcMain.on('main-open-directory', (e) => {
  dialog.showOpenDialog({ properties: ['openDirectory'] }).then((dlg) => {
    let dir: string | null = null;
    if (!dlg.canceled) {
      dir = dlg.filePaths[0];
    }
    e.reply('main-open-directory-picked', dir);
  });
});
