import { join } from 'path';
import { App } from 'electron';
import Store from 'electron-store';

export const initConfig = (
  app: App
): Store<{
  width: unknown;
  height: unknown;
  openLimit: unknown;
  currency: unknown;
  basePath: unknown;
}> => {
  let defaultBasePath = join(app.getPath('appData'), app.name + '_data');

  if (process.platform === 'linux') {
    defaultBasePath = join(app.getPath('home'), app.name + '_data');
  }

  return new Store({
    schema: {
      width: {
        type: 'number',
        maximum: 4096,
        minimum: 500,
        default: 1024
      },
      height: {
        type: 'number',
        maximum: 2048,
        minimum: 300,
        default: 670
      },
      openLimit: {
        type: 'number',
        maximum: 999,
        minimum: 10,
        default: 50
      },
      currency: {
        type: 'string',
        default: 'eur'
      },
      basePath: {
        type: 'string',
        default: defaultBasePath
      }
    }
  });
};
