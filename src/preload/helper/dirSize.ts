import { join } from 'path';
import { lstatSync, readdirSync } from 'fs';

function readSizeRecursive(folder: string): Promise<number> {
  return new Promise((resolve) => {
    const stats = lstatSync(folder);
    let total: number = stats.size;

    const list = readdirSync(folder);
    if (list.length > 0) {
      Promise.all(
        list.map(async (li) => {
          const stat = await lstatSync(join(folder, li));

          if (stat.isDirectory()) {
            const tt = await readSizeRecursive(join(folder, li));
            total += tt;
          } else {
            total += stat.size;
          }
        })
      ).then(() => resolve(total));
    } else {
      resolve(total);
    }
  });
}

export { readSizeRecursive };
