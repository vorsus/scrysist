import MagicCard from '@model/types/MagicCard';
import { ScryfallCard } from '@model/interfaces/Scryfall';

/**
 * Pass only values that may change
 */
export const fillCard = (mc: MagicCard, c: ScryfallCard): void => {
  mc.id = c.id;
  mc.type = c.type_line;
  mc.rarity = c.rarity;
  mc.editionCode = c.set;
  mc.editionName = c.set_name;
  // mc.cardFrame = c.frame;
  // mc.cardBorder = c.border_color;
  mc.scryfallUri = c.scryfall_uri;
  mc.gathererUri = c.related_uris.gatherer;
  mc.inBooster = c.booster;
  mc.finishes = c.finishes;
  mc.language = c.lang;

  // image urls (small + normal)
  if (c.image_uris !== undefined) {
    mc.imageUrlNormal = c.image_uris.normal;
    mc.imageUrlSmall = c.image_uris.small;
    mc.imageUrlArt = c.image_uris.art_crop;
  }

  if (c.card_faces && c.card_faces.length > 0) {
    if (c.card_faces[0].image_uris !== undefined) {
      mc.imageUrlNormal = c.card_faces[0].image_uris.normal;
      mc.imageUrlSmall = c.card_faces[0].image_uris.small;
      mc.imageUrlArt = c.card_faces[0].image_uris.art_crop;
    }
    if (c.card_faces[1].image_uris !== undefined) {
      mc.imageUrlNormalBack = c.card_faces[1].image_uris.normal;
      mc.imageUrlSmallBack = c.card_faces[1].image_uris.small;
    }
  }
};

export const buildCard = (c: ScryfallCard): MagicCard => {
  const card = new MagicCard(c.oracle_id, c.name, c.cmc);
  fillCard(card, c);

  // static values no matter which set
  card.colorIdentity = c.color_identity;
  card.power = c.power;
  card.toughness = c.toughness;
  card.loyalty = c.loyalty;
  card.defense = c.defense;
  card.keywords = c.keywords;
  card.edhrecRank = c.edhrec_rank;
  card.cost = c.mana_cost;
  card.prices = c.prices;

  // search indexed
  card.index = c.name;
  if (c.flavor_name) {
    card.otherNames.push(c.flavor_name);
    card.index += ' ' + c.flavor_name;
  }

  if (c.colors !== undefined) {
    card.colors = c.colors;
  }

  if (c.produced_mana !== undefined) {
    card.producedMana = c.produced_mana;
  }

  let missingOracle = false;
  if (c.oracle_text !== undefined) {
    card.oracleTexts = [c.oracle_text];
  } else {
    missingOracle = true;
  }

  // images and oracle texts
  if (c.card_faces && c.card_faces.length > 0) {
    for (let i = 0; i < c.card_faces.length; i++) {
      if (missingOracle) {
        const cardFaceOracle = c.card_faces[i].oracle_text;

        if (cardFaceOracle !== undefined) {
          card.oracleTexts.push(cardFaceOracle);
          card.costs.push(c.card_faces[i].mana_cost);

          if (card.cost === undefined) {
            card.cost = c.card_faces[i].mana_cost;
          }
        }
      }
    }

    // take stats from face-up card
    if (c.card_faces[0].colors !== undefined) {
      card.colors = card.colors.concat(c.card_faces[0].colors);
    }
    if (c.card_faces[0].power !== undefined) {
      card.power = c.card_faces[0].power;
    }
    if (c.card_faces[0].toughness !== undefined) {
      card.toughness = c.card_faces[0].toughness;
    }
    if (c.card_faces[0].loyalty !== undefined) {
      card.loyalty = c.card_faces[0].loyalty;
    }
    if (c.card_faces[0].defense !== undefined) {
      card.defense = c.card_faces[0].defense;
    }
  }

  if (card.costs.length === 0) {
    card.costs = [c.mana_cost];
  }

  const td = splitTypes(card.type);
  card.superTypes = td.superTypes;
  card.subTypes = td.subTypes;
  card.isLand = td.superTypes.includes('Land');
  card.isPermanent = !td.superTypes.includes('Sorcery') && !td.superTypes.includes('Instant');
  card.isCreature = td.superTypes.includes('Creature');

  return card;
};

const splitTypes = (typeLine: string | undefined): { superTypes: string[]; subTypes: string[] } => {
  let subTypes: string[] = [];
  let superTypes: string[] = [];

  // for the one buggy card ...
  if (typeLine) {
    const idxMultiple = typeLine.indexOf('//');
    if (idxMultiple > 0) {
      typeLine = typeLine.substring(0, idxMultiple - 1);
    }

    const idx = typeLine.indexOf('—');
    if (idx > 0) {
      superTypes = typeLine
        .substring(0, idx - 1)
        .trim()
        .split(' ');
      subTypes = typeLine
        .substring(idx + 1)
        .trim()
        .split(' ');
    } else {
      superTypes = typeLine.trim().split(' ');
    }
  }

  return {
    superTypes,
    subTypes
  };
};

// some examples for different card types
export const debugPrinter = (card: MagicCard, c: ScryfallCard): void => {
  if (card.englishName === 'Boomerang') {
    console.debug('Boomerang', card, c);
  }

  // DEBUG EXAMPLE: double faced
  if (c.id === '0511e232-2a72-40f5-a400-4f7ebc442d17') {
    console.debug('Double Faced Land/Land', card, c);
  }

  if (c.id === 'd8ed0335-daa6-4dbe-a94d-4d56c8cfd093') {
    console.debug('Double Faced Spell/Land)', card, c);
  }

  if (c.oracle_id === 'da5881ca-b86e-4582-ae0e-1570c59773ae') {
    console.debug('Double Faced Transform', card, c);
  }

  if (c.id === '75754468-2850-42e6-ab22-61ff7b9d1214') {
    console.debug('Double Faced Adventure', card, c);
  }

  if (c.id === 'ea164381-1e76-4cfb-ab88-70b97c6285dd') {
    console.debug('Double Faced Two-Side Cast', card, c);
  }

  if (c.id === '06c9e2e8-2b4c-4087-9141-6aa25a506626') {
    console.debug('Double Faced Aftermath', card, c);
  }

  if (c.id === '93f623da-616e-4067-9ea8-5dfbeef8ce0b') {
    console.debug('Double Faced Battle', card, c);
  }

  // DEBUG EXAMPLE: flavour name
  if (c.oracle_id === '0a766368-c982-4cba-9638-c7a56978c240') {
    console.debug('Flavour name (Godzilla)', card, c);
  }

  // DEBUG EXAMPLE: strange Dr. Who planes card
  if (c.id === 'c3cd9db0-b417-42e8-b3be-00fdc9330014') {
    console.debug('Dr. who planes', card, c);
  }

  // DEBUG: special initial print
  if (c.oracle_id === 'a726c27d-2955-4d41-b8a6-fca654c020a2') {
    console.debug('Secret Lair (Eleven)', card, c);
  }
};
