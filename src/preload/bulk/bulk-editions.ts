import Editions from '@model/collection/Editions';
import { BoolCallback } from '@model/interfaces/Callbacks';
import { SryfallSet } from '@model/interfaces/Scryfall';
import MagicEdition from '@model/types/MagicEdition';
import { downloadBulkFile } from './bulk-utils';

export const loadEditions = (editionsContainer: Editions, callback: BoolCallback, path: string): void => {
  downloadBulkFile('editions', path, (err: NodeJS.ErrnoException | null, data: string) => {
    if (err) {
      console.error(path, err);
      callback(false);
    } else {
      const sets = JSON.parse(data);

      sets.data.forEach((s: SryfallSet) => {
        if (
          s.card_count > 0 &&
          !s.digital &&
          s.set_type !== 'funny' &&
          s.set_type !== 'token' &&
          s.set_type !== 'memorabilia' &&
          s.set_type !== 'minigame' &&
          s.set_type !== 'vanguard' &&
          s.code !== 'ohop' // Planeschase Planes
        ) {
          /*
            core: A yearly Magic core set (Tenth Edition, etc)
            expansion: A rotational expansion set in a block (Zendikar, etc)
            masters: A reprint set that contains no new cards (Modern Masters, etc)
            alchemy: An Arena set designed for Alchemy
            masterpiece: Masterpiece Series premium foil cards
            arsenal: A Commander-oriented gift set
            from_the_vault: From the Vault gift sets
            spellbook: Spellbook series gift sets
            premium_deck: Premium Deck Series decks
            duel_deck: Duel Decks
            draft_innovation: Special draft sets, like Conspiracy and Battlebond
            treasure_chest: Magic Online treasure chest prize sets
            commander: Commander preconstructed decks
            planechase: Planechase sets
            archenemy: Archenemy sets
            vanguard: Vanguard card sets
            funny: A funny un-set or set with funny promos (Unglued, Happy Holidays, etc)
            starter: A starter/introductory set (Portal, etc)
            box: A gift box set
            promo: A set that contains purely promotional cards
            token: A set made up of tokens and emblems.
            memorabilia: A set made up of gold-bordered, oversize, or trophy cards that are not legal
            minigame: A set that contains minigame card inserts from booster packs
          */
          const cip = 'cip://edition-svg/' + s.code + '@' + s.icon_svg_uri;
          const edition = new MagicEdition(s.id, s.code, s.name, s.card_count, s.released_at, s.scryfall_uri, s.set_type, s.icon_svg_uri, cip);

          editionsContainer.add(edition);
        }
      });

      callback(true);
    }
  });
};
