import Editions from '@model/collection/Editions';
import searchApi from '../indices/search';
import { BASIC_LANDS } from '@model/enums';
import { buildCard, debugPrinter } from './bulk-cards-common';
import { downloadBulkFile } from './bulk-utils';
import { ScryfallCard } from '@model/interfaces/Scryfall';
import { BoolCallback } from '@model/interfaces/Callbacks';

/* **************************************************************************************************
 * Read only oracle cards first to get the most common version of a card
 * Unfortunately many cards are missing which have only one printing
 * So fetch default cards in addition, add missing cards and update flavor name if any
 * Also fix some bugs with the oracle list, replace commander cards as "Yavimaya, Cradle of Growth"
 * *********************************************************************************************** */

const brokenCards = new Set();
brokenCards.add('09623b0c-a771-46e3-b7c0-d4097486f1cd'); // Boomerang 8th Edition image is broken

export const loadCardsIntoIndex = (
  search: typeof searchApi,
  editionsContainer: Editions,
  completeCallback: BoolCallback,
  pathOracle: string,
  pathDefault: string
): void => {
  const foundCards = new Set();

  const readCallback = (err: NodeJS.ErrnoException | null, data: string): void => {
    if (err) {
      console.error(pathOracle, err);
      completeCallback(false);
    } else {
      const cards = JSON.parse(data);

      cards.forEach((c: ScryfallCard) => {
        if (c.digital === true || c.oversized === true || c.layout === 'token' || c.layout === 'emblem') {
          return;
        }

        if (!editionsContainer.has(c.set)) {
          return;
        }

        if (c.name in BASIC_LANDS) {
          return;
        }

        if (brokenCards.has(c.id)) {
          return;
        }

        const card = buildCard(c);

        try {
          search.addCardToIndex(card);
          foundCards.add(c.oracle_id);
        } catch (error) {
          console.error(error, c);
        }

        debugPrinter(card, c);
      });

      readCallbackMore();
    }
  };

  const readCallbackMore = (): void => {
    downloadBulkFile('default_cards', pathDefault, (err: NodeJS.ErrnoException | null, data: string) => {
      if (err) {
        console.error(pathDefault, err);
        completeCallback(false);
      } else {
        const cards = JSON.parse(data);

        cards.forEach((c: ScryfallCard) => {
          if (c.digital === true || c.oversized === true || c.layout === 'token' || c.layout === 'emblem') {
            return;
          }

          if (!editionsContainer.has(c.set)) {
            return;
          }

          if (c.name in BASIC_LANDS && BASIC_LANDS[c.name] !== c.id) {
            return;
          }

          if (brokenCards.has(c.id)) {
            return;
          }

          if (c.flavor_name || !foundCards.has(c.oracle_id)) {
            let card = search.fetchCardByOracleId(c.oracle_id);

            if (null === card) {
              card = buildCard(c);
              try {
                search.addCardToIndex(card);
              } catch (error) {
                console.error(error, c);
              }

              debugPrinter(card, c);
            } else {
              // foreign black border cards
              if ('en' !== card.language) {
                // keep lokijs id and counter (unique)
                const savedCounter = card.counter;
                const newCard = Object.assign(card, buildCard(c));
                card.counter = savedCounter;

                /* if (card.otherNames.length > 0) {
                  newCard.otherNames.concat(card.otherNames);
                  newCard.index += ' ' + card.index;
                } */

                try {
                  search.updateCardInIndex(newCard);
                } catch (error) {
                  console.error(error, c, newCard);
                }
              } else if (c.flavor_name) {
                card.otherNames.push(c.flavor_name);
                card.index += ' ' + c.flavor_name;

                try {
                  search.updateCardInIndex(card);
                } catch (error) {
                  console.error(error, c);
                }
              }
            }
          }
        });

        completeCallback(true);
      }
    });
  };

  downloadBulkFile('oracle_cards', pathOracle, readCallback);
};
