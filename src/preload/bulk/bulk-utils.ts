import { readFile, writeFile, existsSync } from 'fs';
import { promisify } from 'util';
const writeFilePromise = promisify(writeFile);

type DownloadCallback = (err: NodeJS.ErrnoException | null, data: string) => void;

const readLocal = (outputPath: string, readCallback: DownloadCallback): void => {
  readFile(outputPath, <(err: NodeJS.ErrnoException | null, data: Buffer) => void>(<unknown>readCallback));
};

const getUrl = async (type: string): Promise<string> => {
  switch (type) {
    case 'editions':
      return 'https://api.scryfall.com/sets';
    case 'symbols':
      return 'https://api.scryfall.com/symbology';
  }

  // for oracle_cards and default_cards we have to fetch dl link first
  const response = await fetch('https://api.scryfall.com/bulk-data');
  const json = await response.json();

  /*
    {
      "object": "bulk_data",
      "id": "27bf3214-1271-490b-bdfe-c0be6c23d02e",
      "type": "oracle_cards",
      "updated_at": "2023-06-16T09:02:43.468+00:00",
      "uri": "https://api.scryfall.com/bulk-data/27bf3214-1271-490b-bdfe-c0be6c23d02e",
      "name": "Oracle Cards",
      "description": "A JSON file containing one Scryfall card object for each Oracle ID on Scryfall. The chosen sets for the cards are an attempt to return the most up-to-date recognizable version of the card.",
      "size": 116936701,
      "download_uri": "https://data.scryfall.io/oracle-cards/oracle-cards-20230616090243.json",
      "content_type": "application/json",
      "content_encoding": "gzip"
    },
  */
  for (let i = 0; i < json.data.length; i++) {
    if (json.data[i].type === type) {
      return json.data[i].download_uri;
    }
  }

  throw new Error('URL not found.');
};

const downloadBulkFile = (type: string, outputPath: string, readCallback: DownloadCallback): void => {
  if (!existsSync(outputPath)) {
    getUrl(type).then((url) => {
      fetch(url)
        .then((x) => x.arrayBuffer())
        .then((x) => writeFilePromise(outputPath, Buffer.from(x)).then(() => readLocal(outputPath, readCallback)));
    });
  } else {
    readLocal(outputPath, readCallback);
  }
};

export { downloadBulkFile };
