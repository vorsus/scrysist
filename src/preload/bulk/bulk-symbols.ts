import MagicSymbol from '@model/types/MagicSymbol';
import Symbols from '@model/collection/Symbols';
import { downloadBulkFile } from './bulk-utils';
import { ScryfallSymbol } from '@model/interfaces/Scryfall';
import { BoolCallback } from '@model/interfaces/Callbacks';

// b64 is not safe on windows (incasesensitive fs)
const base32 = require('base32');

export const loadSymbols = (symbolsContainer: Symbols, callback: BoolCallback, path: string): void => {
  downloadBulkFile('symbols', path, (err: NodeJS.ErrnoException | null, data: string) => {
    if (err) {
      console.error(path, err);
      callback(false);
    } else {
      const symbology = JSON.parse(data);

      for (const key in symbology.data) {
        const s = symbology.data[key] as ScryfallSymbol;
        /*
          symbol	"{C}"
          svg_uri	"https://svgs.scryfall.io/card-symbols/C.svg"
          loose_variant	"C"
          english	"one colorless mana"
          transposable	false
          represents_mana	true
          appears_in_mana_costs	true
          mana_value	1
          cmc	1
          funny	false
          colors	[]
          gatherer_alternates	null
        */
        const cip = 'cip://symbol-svg/' + base32.encode(s.symbol) + '@' + s.svg_uri;
        const symbol = new MagicSymbol(s.symbol, s.english, s.svg_uri, cip);

        symbolsContainer.add(symbol);
      }

      callback(true);
    }
  });
};
