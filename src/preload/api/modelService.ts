import searchApi from '../indices/search';
import registerApi from '../indices/register';
import Editions from '@model/collection/Editions';
import Symbols from '@model/collection/Symbols';
import Storages from '@model/collection/Storages';
import MagicCard from '@model/types/MagicCard';
import MagicSymbol from '@model/types/MagicSymbol';
import MagicEdition from '@model/types/MagicEdition';
import MagicTag from '@model/types/MagicTag';
import { StorageFile, StorageSlot, StorageValues } from '@model/interfaces/Storage';
import DeckRegister from '@model/types/DeckRegister';

export type ModelService = {
  Editions: {
    getList: () => MagicEdition[];
    get: (code: string) => MagicEdition;
  };
  Symbols: {
    getList: () => MagicSymbol[];
    get: (code: string) => MagicSymbol;
  };
  Decks: {
    getDataList: () => { [index: string]: StorageFile };
    getData: (path: string) => StorageFile;
    setField: (path: string, key: string, value: string | string[] | StorageSlot[] | number | null) => void;
    sync: (path: string, updatedFields: { [index: string]: StorageValues }) => void;
    has: (path: string) => boolean;
    findByOracleId: (oracleId: string) => DeckRegister[];
    findByTags: (tags: string[]) => DeckRegister[];
    findAll: () => DeckRegister[];
  };
  Cards: {
    search: (text: string) => MagicCard[];
    byName: (name: string) => MagicCard | null;
    byOracleId: (oracleId: string) => MagicCard | null;
    byId: (id: string) => MagicCard | null;
    byEdition: (code: string) => MagicCard[];
    byCounter: (counter: number) => MagicCard | null;
    total: () => number;
  };
  Tags: {
    getAutoList: () => MagicTag[];
  };
};

export const initModelService = (editions: Editions, symbols: Symbols, storages: Storages): ModelService => {
  return {
    Editions: {
      getList: () => editions.getList(),
      get: (code) => editions.get(code)
    },
    Symbols: {
      getList: () => symbols.getList(),
      get: (code) => symbols.get(code)
    },
    Decks: {
      getDataList: () => storages.getDataList(),
      getData: (path) => storages.getData(path),
      setField: (path, key, value) => storages.setField(path, key, value),
      sync: (path) => {
        storages.sync(path);

        setTimeout(() => {
          registerApi.reloadPath(path);
        }, 300);
      },
      has: (path) => storages.has(path),
      findByOracleId: (oracleId) => registerApi.findDecksByOracleId(oracleId),
      findByTags: (tags) => registerApi.findDecksByTags(tags),
      findAll: () => registerApi.getAllDecks()
    },
    Cards: {
      search: (text) => searchApi.searchCardByText(text),
      byName: (name) => searchApi.fetchCardByName(name),
      byOracleId: (oracleId) => searchApi.fetchCardByOracleId(oracleId),
      byId: (id) => searchApi.fetchCardById(id),
      byEdition: (code) => searchApi.fetchCardsByEdition(code),
      byCounter: (counter) => searchApi.fetchCardByCounter(counter),
      total: () => searchApi.countTotalCards()
    },
    Tags: {
      getAutoList: () => searchApi.fetchTags()
    }
  } as ModelService;
};
