import { ipcRenderer } from 'electron';
import { join } from 'path';
import { loadSymbols } from '../bulk/bulk-symbols';
import { loadEditions } from '../bulk/bulk-editions';
import { loadCardsIntoIndex } from '../bulk/bulk-cards';
import { loadTree } from '../decks/tree';
import { createExamples } from '../decks/examples';
import fileActionApi from '../decks/fileActions';
import searchApi from '../indices/search';
import registerApi from '../indices/register';
import Symbols from '@model/collection/Symbols';
import Storages from '@model/collection/Storages';
import Editions from '@model/collection/Editions';
import { FileTreeNode } from '@model/interfaces/Storage';
import { DeckLoadedCallback } from '@model/interfaces/Callbacks';

export type DataService = {
  loadBulk: () => void;
  resetBulk: () => void;
  loadDecks: () => void;
  readSettings: () => void;
  reloadDecks: (anotherCallback?: DeckLoadedCallback) => void;
  createExampleDecks: () => void;
};

export const initDataService = (editions: Editions, symbols: Symbols, storages: Storages, basePath: string): DataService => {
  return {
    loadBulk: () => {
      searchApi.initIndex(join(basePath, 'cache', 'search-index.db'), (rebuild: boolean) => {
        loadEditions(
          editions,
          (state) => {
            ipcRenderer.emit('editions-loaded', state);
            if (rebuild) {
              loadCardsIntoIndex(
                searchApi,
                editions,
                (state) => {
                  console.info('Cards loaded!', new Date());
                  searchApi.saveIndexDatabase();
                  ipcRenderer.emit('cards-loaded', state);
                },
                join(basePath, 'bulk', 'oracle-cards.json'),
                join(basePath, 'bulk', 'default-cards.json')
              );
            } else {
              console.info('Cards loaded!', new Date());
              ipcRenderer.emit('cards-loaded', state);
            }
          },
          join(basePath, 'bulk', 'sets.json')
        );
      });
      loadSymbols(symbols, (state) => ipcRenderer.emit('symbols-loaded', state), join(basePath, 'bulk', 'symbology.json'));
    },
    resetBulk: () => {
      try {
        fileActionApi.delete(join(basePath, 'bulk', 'sets.json'), 'file');
        fileActionApi.delete(join(basePath, 'bulk', 'symbology.json'), 'file');
        fileActionApi.delete(join(basePath, 'bulk', 'default-cards.json'), 'file');
        fileActionApi.delete(join(basePath, 'bulk', 'oracle-cards.json'), 'file');
        fileActionApi.delete(join(basePath, 'cache', 'search-index.db'), 'file');
      } catch (err) {
        console.error(err);
      }
    },
    loadDecks: () => {
      loadTree(
        storages,
        (root: string, fileSystem: FileTreeNode[], flatten: string[]) => {
          registerApi.asyncInitIndex(root, join(basePath, 'cache', 'register_index.db'), flatten, (tagList) => {
            ipcRenderer.emit('decks-loaded', true, root, fileSystem, flatten, tagList);
          });
        },
        join(basePath, 'decks')
      );
    },
    reloadDecks: (anotherCallback?) => {
      // quick fix, the callback passed to init is cached and fired on updatePath
      if (anotherCallback) {
        ipcRenderer.once('decks-reloaded', (state, root, fileSystem, flatten, tagList) =>
          anotherCallback(<boolean>(<unknown>state), root, fileSystem, flatten, tagList)
        );
      }
      loadTree(
        storages,
        (root: string, fileSystem: FileTreeNode[], flatten: string[]) => {
          registerApi.asyncInitIndex(root, join(basePath, 'cache', 'register_index.db'), flatten, (tagList) => {
            ipcRenderer.emit('decks-reloaded', true, root, fileSystem, flatten, tagList);
          });
        },
        join(basePath, 'decks')
      );
    },
    createExampleDecks: () => {
      createExamples(join(basePath, 'decks'));
    }
  } as DataService;
};
