import { ipcRenderer } from 'electron';
import fileActionApi from '../decks/fileActions';
import { join } from 'path';
import { readSizeRecursive } from '../helper/dirSize';

export type FileSystem = {
  onError: (callback: (msg: string) => void) => void;
  moveFolder: (source: string, target: string) => boolean;
  renameFolder: (path: string, newName: string) => boolean;
  deleteFolder: (path: string) => boolean;
  addFolder: (name: string, parentPath: string) => boolean;
  moveFile: (source: string, target: string) => boolean;
  renameFile: (path: string, newName: string) => boolean;
  deleteFile: (path: string) => boolean;
  addFile: (name: string, parentPath: string, content: string) => string | boolean;
  openShell: (path: string, type?: 'file' | 'folder') => void;
  cacheDirSize: (callback: (size: number) => void) => void;
};

export const initFileSystem = (basePath: string): FileSystem => {
  return {
    onError: (callback): void => {
      ipcRenderer.on('file-operation-error', (msg) => callback(<string>(<unknown>msg)));
    },
    moveFolder: (source: string, target: string): boolean => {
      try {
        fileActionApi.move(source, target, 'folder');

        return true;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    renameFolder: (path: string, newName: string): boolean => {
      try {
        fileActionApi.rename(path, newName, 'folder');

        return true;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    deleteFolder: (path: string): boolean => {
      try {
        fileActionApi.delete(path, 'folder');

        return true;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    addFolder: (name: string, parentPath: string): boolean => {
      try {
        fileActionApi.create(name, parentPath, null, 'folder');

        return true;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    moveFile: (source: string, target: string): boolean => {
      try {
        fileActionApi.move(source, target, 'file');

        return true;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    renameFile: (path: string, newName: string): boolean => {
      try {
        fileActionApi.rename(path, newName, 'file');

        return true;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    deleteFile: (path: string): boolean => {
      try {
        fileActionApi.delete(path, 'file');

        return true;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    addFile: (name: string, parentPath: string, content: string): string | boolean => {
      try {
        const newPath = fileActionApi.create(name, parentPath, content, 'file');

        return newPath;
      } catch (err) {
        console.error(err);
        ipcRenderer.emit('file-operation-error', err);

        return false;
      }
    },
    openShell: (path: string, type: 'file' | 'folder' = 'file'): void => {
      if (type === 'folder') {
        fileActionApi.openShellFolder(path);
      } else {
        fileActionApi.openShellFile(path);
      }
    },
    cacheDirSize: (callback: (size: number) => void): void => {
      readSizeRecursive(join(basePath, 'cache')).then((size) => {
        callback(size);
      });
    }
  } as FileSystem;
};
