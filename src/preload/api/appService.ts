import { ipcRenderer } from 'electron';
import { sep, join, dirname } from 'path';
import AppSettings from '@model/types/AppSettings';
import { Currency } from '@model/enums';
import icon from '../../../resources/icon.png?asset&asarUnpack';

export type AppService = {
  getOpenLimit: () => number;
  getCurrency: () => Currency;
  pathSeparator: () => string;
  decksRootPath: () => string;
  getRootCssPath: (fileName: string) => string;
  getThemeCssPath: (themeName: string, fileName: string) => string;
  readSettings: () => void;
  updateSettings: (appSettings: AppSettings) => void;
  restartApp: () => void;
  openConsole: () => void;
  pickDirectory: (callback: (dir: string | null) => void) => void;
};

export const initAppService = (basePath: string, openLimit: number, currency: Currency): AppService => {
  return {
    getOpenLimit: () => openLimit,
    getCurrency: () => currency,
    getRootCssPath: (fileName: string): string => join(dirname(icon), 'themes', fileName + '.css'),
    getThemeCssPath: (themeName: string, fileName: string): string => join(dirname(icon), 'themes', themeName, fileName + '.css'),
    pathSeparator: () => sep,
    decksRootPath: () => join(basePath, 'decks'),
    readSettings: () => {
      const appSettings = new AppSettings(basePath, openLimit, currency);
      ipcRenderer.emit('settings-read', appSettings);
    },
    updateSettings: (appSettings): void => {
      ipcRenderer.invoke('main-update-settings', appSettings.basePath, appSettings.openLimit, appSettings.currency);
    },
    restartApp: () => {
      ipcRenderer.invoke('main-relaunch');
    },
    openConsole: () => {
      ipcRenderer.invoke('main-open-console');
    },
    pickDirectory: (callback) => {
      ipcRenderer.once('main-open-directory-picked', (_ev, dir: string | null) => callback(dir));
      ipcRenderer.send('main-open-directory');
    }
  } as AppService;
};
