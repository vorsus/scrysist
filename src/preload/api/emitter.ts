import { ipcRenderer } from 'electron';
import { BoolCallback, DeckLoadedCallback } from '@model/interfaces/Callbacks';
import AppSettings from '@model/types/AppSettings';

export type Emitter = {
  onEditionsLoaded: (callback: BoolCallback) => void;
  onSymbolsLoaded: (callback: BoolCallback) => void;
  onCardsLoaded: (callback: BoolCallback) => void;
  onDecksLoaded: (callback: DeckLoadedCallback) => void;
  onDecksReloaded: (callback: DeckLoadedCallback) => void;
  onSettingsRead: (callback: (appSettings: AppSettings) => void) => void;
};

export const initEmitter = (): Emitter => {
  return {
    onEditionsLoaded: (callback): void => {
      ipcRenderer.on('editions-loaded', (state) => callback(<boolean>(<unknown>state)));
    },
    onSymbolsLoaded: (callback): void => {
      ipcRenderer.on('symbols-loaded', (state) => callback(<boolean>(<unknown>state)));
    },
    onCardsLoaded: (callback): void => {
      ipcRenderer.on('cards-loaded', (state) => callback(<boolean>(<unknown>state)));
    },
    onDecksLoaded: (callback): void => {
      ipcRenderer.on('decks-loaded', (state, root, tree, flatten, tagList) => callback(<boolean>(<unknown>state), root, tree, flatten, tagList));
    },
    onDecksReloaded: (callback): void => {
      ipcRenderer.on('decks-reloaded', (state, root, tree, flatten, tagList) => callback(<boolean>(<unknown>state), root, tree, flatten, tagList));
    },
    onSettingsRead: (callback): void => {
      ipcRenderer.on('settings-read', (appSettings) => callback(<AppSettings>(<unknown>appSettings)));
    }
  } as Emitter;
};
