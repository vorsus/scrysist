import { existsSync, mkdirSync, readFileSync, writeFileSync } from 'fs';
import { dirname, join } from 'path';
import icon from '../../../resources/icon.png?asset&asarUnpack';

// for less struggle just keep the list hard coded
const exampleDirList = ['EDH', 'Legacy', 'Pro Tour', 'Community'];
const exampleFilesList = [
  join('EDH', 'Heads I Win, Tails You Lose.json'),
  join('Legacy', 'Eldrazi Stompy.json'),
  join('Legacy', 'Miracles.json'),
  join('Legacy', 'Termur Delver.json'),
  join('Pro Tour', 'Abzan Midrange.json'),
  join('Pro Tour', 'Grixis Control.json'),
  join('Pro Tour', 'Mono-Black Zombies.json'),
  join('Pro Tour', 'Monoblue Devotion.json'),
  join('Pro Tour', 'Sultai Energy.json'),
  join('Community', 'Mardu Flashback.json')
];

export const createExamples = (targetPath: string): void => {
  // we cannot import any folders or json filesd without side effects
  // so take the icon in the root, we just want the resource folder here
  const examplesRoot = join(dirname(icon), 'examples');

  exampleDirList.forEach((d) => {
    if (!existsSync(join(targetPath, d))) {
      mkdirSync(join(targetPath, d));
    }
  });

  exampleFilesList.forEach((f) => {
    if (!existsSync(join(targetPath, f))) {
      const content = readFileSync(join(examplesRoot, f), 'utf-8');
      writeFileSync(join(targetPath, f), content, {
        encoding: 'utf8'
      });
    }
  });
};
