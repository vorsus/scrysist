import { renameSync, realpathSync, existsSync, rmdirSync, rmSync, writeFileSync, mkdirSync } from 'fs';
import { basename, join } from 'path';
import { shell } from 'electron';

export default {
  rename: (path: string, newName: string, type: 'file' | 'folder'): void => {
    const basePath = realpathSync(join(path, '..'));

    const re = /^(con|prn|aux|nul|com[1-9]|lpt[1-9])$|([<>:"/\\|?*])|(\.|\s)$/gi;
    if (re.test(newName)) {
      throw Error('Filename contains illegal characters!');
    }

    if ('file' === type) {
      newName += '.json'; // rename dialog cuts the extension
    } else {
      // folder
      if (newName.indexOf('.json') !== -1) {
        throw Error('Folder must not be named as json!');
      }
    }

    console.info('Rename ' + type + ': ' + path + ' to ' + basePath + '/' + newName);

    if (existsSync(join(basePath, newName))) {
      throw Error('File with same name already exists!');
    }

    renameSync(path, join(basePath, newName));
  },
  move: (source: string, target: string, type: 'file' | 'folder'): void => {
    const sourceBaseName = basename(source);
    const finalTarget = join(target, sourceBaseName);

    console.info('Move ' + type + ': ' + source + ' to ' + finalTarget);

    if (existsSync(finalTarget)) {
      throw Error('Cannot move: target already exists');
    }

    renameSync(source, finalTarget);
  },
  delete: (path: string, type: 'file' | 'folder'): void => {
    console.warn('Delete ' + type + ': ' + path);

    if ('file' === type) {
      rmSync(path);
    } else {
      rmdirSync(path, {
        recursive: true
      });
    }
  },
  create: (name: string, parentPath: string, content: string | null, type: 'file' | 'folder'): string => {
    const re = /^(con|prn|aux|nul|com[1-9]|lpt[1-9])$|([<>:"/\\|?*])|(\.|\s)$/gi;
    if (re.test(name)) {
      throw Error('Filename contains illegal characters!');
    }

    let newPath: string;
    if ('file' === type) {
      newPath = join(parentPath, name + '.json');
      if (existsSync(newPath)) {
        throw Error('Filename already exists!');
      }

      if (content) {
        writeFileSync(newPath, content, {
          encoding: 'utf8'
        });
      }
    } else {
      newPath = join(parentPath, name);
      mkdirSync(join(parentPath, name));
    }

    return newPath;
  },
  openShellFile: (path: string): void => {
    shell.showItemInFolder(path);
  },
  openShellFolder: (path: string): void => {
    shell.openPath(path);
  }
};
