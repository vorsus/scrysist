import Storages from '@model/collection/Storages';
import { CardColorIndicators } from '@model/icons/mana';
import { FileTreeNode } from '@model/interfaces/Storage';
import StorageAdapter from '@model/types/StorageAdapter';
import JSONdb from 'simple-json-db';
import dirTree from 'directory-tree';
import { isSpecialPath } from '@model/enums';
import { sep } from 'path';

export type FileTreeCallback = (path: string, fileSystem: FileTreeNode[], flatten: string[]) => void;

interface VendorDirectoryTreeNode {
  path: string;
  name: string;
  children: VendorDirectoryTreeNode[];
}

export const loadTree = (storages: Storages, callback: FileTreeCallback, path: string): void => {
  const tree = dirTree(path);

  // build file tree for view
  const parseChildren = (children: VendorDirectoryTreeNode[], flatten: string[]): FileTreeNode[] => {
    const branches: FileTreeNode[] = [];
    for (const key in children) {
      const node = children[key];
      const leaf = node.path.endsWith('.json');
      let icon = leaf ? 'pi pi-fw pi-th-large' : 'pi pi-fw pi-folder';
      let colors = [];
      let flags = [];
      let isSpecial = false;

      if (leaf) {
        flatten.push(node.path);
      }

      // load and parse json
      if (leaf) {
        try {
          const db = new JSONdb(node.path, { syncOnWrite: false });
          storages.add(new StorageAdapter(node.path, db));

          colors = db.get('colors');
          if (colors) {
            icon = CardColorIndicators.iconClass(colors);
          }

          flags = db.get('flags');
        } catch (err) {
          console.error(err, node.path);
        }
      }

      isSpecial = isSpecialPath(node.path, path, sep);

      branches.push({
        label: node.name.replace(/.json/, ''),
        key: node.path,
        icon,
        leaf,
        children: parseChildren(node.children, flatten),
        expandedIcon: true,
        collapsedIcon: true,
        colors,
        flags,
        isSpecial
      });
    }

    return branches;
  };

  const flatten: string[] = [];
  let fileSystem: FileTreeNode[] = [];

  if (tree.children) {
    const treeChildrenn = <VendorDirectoryTreeNode[]>(<unknown>tree.children);
    fileSystem = parseChildren(treeChildrenn, flatten);
  }

  callback(path, fileSystem, flatten);
};
