import { ElectronAPI } from '@electron-toolkit/preload';
import { AppService } from './api/appService';
import { DataService } from './api/dataService';
import { ModelService } from './api/modelService';
import { FileSystem } from './api/fileSystem';
import { Emitter } from './api/emitter';

declare global {
  interface Window {
    electron: ElectronAPI;

    fileSystem: FileSystem;
    emitter: Emitter;
    modelService: ModelService;
    dataService: DataService;
    appService: AppService;
  }
}
