import { contextBridge } from 'electron';
import { electronAPI } from '@electron-toolkit/preload';
import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';

import { initEmitter } from './api/emitter';
import { initFileSystem } from './api/fileSystem';
import { initDataService } from './api/dataService';
import { initAppService } from './api/appService';
import { initModelService } from './api/modelService';

import EditionContainer from '../model/collection/Editions';
import SymbolContainer from '../model/collection/Symbols';
import DeckContainer from '../model/collection/Storages';

// look for our options (position is different on windows than on linux)
let argsJson = '{}';
for (let o = 0; o < process.argv.length; o++) {
  if (process.argv[o].indexOf('--options') === 0) {
    argsJson = process.argv[o].substring(10).trim();
    break;
  }
}

const { basePath, openLimit, currency } = JSON.parse(argsJson);
const editions = new EditionContainer();
const symbols = new SymbolContainer();
const decks = new DeckContainer();

// be sure all sub directories are created
existsSync(basePath) || mkdirSync(basePath);
existsSync(join(basePath, 'decks')) || mkdirSync(join(basePath, 'decks'));
existsSync(join(basePath, 'cache')) || mkdirSync(join(basePath, 'cache'));
existsSync(join(basePath, 'bulk')) || mkdirSync(join(basePath, 'bulk'));

const emitter = initEmitter();
const fileSystem = initFileSystem(basePath);
const dataService = initDataService(editions, symbols, decks, basePath);
const appService = initAppService(basePath, openLimit, currency);
const modelService = initModelService(editions, symbols, decks);

try {
  contextBridge.exposeInMainWorld('electron', electronAPI);
  contextBridge.exposeInMainWorld('modelService', modelService);
  contextBridge.exposeInMainWorld('fileSystem', fileSystem);
  contextBridge.exposeInMainWorld('emitter', emitter);
  contextBridge.exposeInMainWorld('dataService', dataService);
  contextBridge.exposeInMainWorld('appService', appService);
} catch (error) {
  console.error(error);
}
