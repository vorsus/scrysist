import { readFile } from 'fs';
import DeckRegister from '@model/types/DeckRegister';
import MagicTag, { formatTag } from '@model/types/MagicTag';
import { TAG_TYPES, isSpecialPath } from '@model/enums';
import { StorageFile } from '@model/interfaces/Storage';
import { sep } from 'path';

const loki = require('lokijs');

type TagListCallback = (tags: MagicTag[]) => void;

let db: typeof loki = null;
let collectionDecks: typeof loki = null;
let loadingCount = 0;
let loadCallback: TagListCallback | null = null;
const tagList: { [index: string]: MagicTag } = {};

const asyncInitIndex = (root: string, path: string, flattenPaths: string[], tagListCallback: TagListCallback): void => {
  db = new loki(path, {
    autoload: true,
    autoloadCallback: (): void => {
      // this is so fast, that it is safer to drop the index on each load to clear wasted entries
      try {
        db.removeCollection('decks');
      } catch (error) {
        console.warn(error);
      }

      collectionDecks = db.getCollection('decks');
      if (collectionDecks === null) {
        collectionDecks = db.addCollection('decks', {
          indices: ['path', 'oracleIds', 'tags']
        });
        // console.log('Create register index', new Date());
      } else {
        // console.log('Found register index', new Date());
      }

      rebuildIndex(root, flattenPaths, tagListCallback);
    }
  });
};

const findDecksByOracleId = (oracleId: string): DeckRegister[] => {
  return collectionDecks.find({
    $or: [{ oracleIds: { $contains: oracleId } }, { companions: { $contains: oracleId } }]
  });
};

const findDecksByTags = (tags: string[]): DeckRegister[] => {
  const result = collectionDecks.find({
    tags: { $containsAny: tags }
  });

  // console.log('SEARCH RESULT BY TAGS', result, tags);

  return result;
};

const getAllDecks = (): DeckRegister[] => {
  return collectionDecks.find();
};

const reloadPath = (path: string): void => {
  rebuildPath(path);
};

const removePath = (path: string): void => {
  collectionDecks.remove({ path });
};

export default {
  asyncInitIndex,
  findDecksByOracleId,
  findDecksByTags,
  getAllDecks,
  reloadPath,
  removePath
};

function rebuildIndex(root: string, flattenPaths: string[], tagListCallback: TagListCallback): void {
  loadCallback = tagListCallback;

  const relevantPaths = flattenPaths.filter((flattenPath) => !isSpecialPath(flattenPath, root, sep));
  if (relevantPaths.length > 0) {
    loadingCount = relevantPaths.length;
    relevantPaths.forEach((path) => {
      rebuildPath(path);
    });
  } else {
    pushLoaded(); // in case no decks available
  }
}

function createRegister(deck: StorageFile, path: string, ts: number): DeckRegister {
  const register = new DeckRegister(path, ts);

  if (deck.formats) {
    register.formats = deck.formats;
  }

  if (deck.colors) {
    register.colors = deck.colors;
  }

  if (deck.companions) {
    register.companions = deck.companions;
  }

  if (deck.level) {
    register.level = deck.level;
  }

  if (deck.type) {
    register.type = deck.type;
  }

  if (deck.tags) {
    register.tags = deck.tags.map((tagName: string) => formatTag(tagName));
  }

  deck.slots.forEach((slot) => {
    if (slot.oracleId && slot.board !== 'consider') {
      register.oracleIds.push(slot.oracleId);
      register.slots.push({ oracleId: slot.oracleId, quantity: slot.quantity, name: slot.name, board: slot.board! });
    }
  });

  return register;
}

function rebuildPath(path: string): void {
  const result = collectionDecks.find({ path });
  // const stats = statSync(path);
  // const ts = Date.parse(stats.mtime.toDateString());

  if (result && result.length > 0) {
    /* if (result[0].ts >= ts) {
      addTagToList(result[0]);
      pushLoaded();
      return;
    } */

    collectionDecks.remove(result[0].$loki);
  }

  // async
  readFile(path, 'utf8', (error, data) => {
    if (error) {
      console.error(error);
      return;
    }

    try {
      const obj = createRegister(JSON.parse(data), path, 0); // , ts
      collectionDecks.insert(obj);
      addTagToList(obj);
      pushLoaded();
    } catch (err) {
      console.error(err);
    }
  });
}

function addTagToList(deckRegister: DeckRegister): boolean {
  let added = false;
  deckRegister.tags.forEach((tagName) => {
    if (!(tagName in tagList)) {
      const fn = formatTag(tagName);
      tagList[fn] = new MagicTag(fn, TAG_TYPES.CUSTOM);
      added = true;
    }
  });

  return added;
}

function pushLoaded(): void {
  loadingCount--;
  if (0 >= loadingCount && loadCallback) {
    loadCallback(Object.values(tagList));
  }
}
