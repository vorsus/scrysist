import MagicCard from '@model/types/MagicCard';
import { COLOR_NAMES, TAG_TYPES } from '@model/enums';
import MagicTag, { formatTag } from '@model/types/MagicTag';
import { BoolCallback } from '@model/interfaces/Callbacks';

const loki = require('lokijs');

let db: typeof loki = null;
let collectionCards: typeof loki = null;
let collectionTags: typeof loki = null;
let cardsCounter = 1;

const appendFields = (card: MagicCard): void => {
  card.cipNormal = 'cip://card-normal/' + card.id + '@' + card.imageUrlNormal;
  card.cipSmall = 'cip://card-small/' + card.id + '@' + card.imageUrlSmall;
  card.cipArt = 'cip://card-art/' + card.id + '@' + card.imageUrlArt;

  if (card.imageUrlNormalBack) {
    card.cipNormalBack = 'cip://card-normal-back/' + card.id + '@' + card.imageUrlNormalBack;
  }

  if (card.imageUrlSmallBack) {
    card.cipSmallBack = 'cip://card-small-back/' + card.id + '@' + card.imageUrlSmallBack;
  }
};

const initIndex = (path: string, rebuildCallback: BoolCallback): void => {
  db = new loki(path, {
    autoload: true,
    autoloadCallback: (): void => {
      let changed = false;
      collectionCards = db.getCollection('cards');
      collectionTags = db.getCollection('tags');

      if (collectionCards === null) {
        collectionCards = db.addCollection('cards', {
          indices: ['index', 'editionCode'],
          unique: ['oracleId', 'englishName', 'counter']
        });
        console.log('Create search index', new Date());
        changed = true;
      } else {
        console.log('Found search index', new Date());
      }

      if (collectionTags === null) {
        collectionTags = db.addCollection('tags', {
          indices: ['ref_count'],
          unique: ['name']
        });
        console.log('Create tags index', new Date());
        changed = true;
      } else {
        console.log('Found tags index', new Date());
      }

      rebuildCallback(changed);
    }
  });
};

const saveIndexDatabase = (): void => {
  if (db !== null) {
    console.log('Save search index database', new Date());
    db.saveDatabase();
  }
};

const searchCardByText = (text: string): MagicCard[] => {
  const expr: { index: object }[] = [];
  let firstTerm = '';
  text = text.replace(/-/g, ' ');

  text.split(' ').forEach((term) => {
    if (firstTerm === '') {
      firstTerm = term;
    }

    const re = new RegExp(term.replace(/[,.]/g, ''), 'i');
    expr.push({ index: { $regex: re } });
  });

  const result = collectionCards.chain().find({ $and: expr }).limit(100).data();

  if (result) {
    result.map(appendFields);
  }

  // sort by name
  /* result.sort((a: MagicCard, b: MagicCard) => {
    if (a.englishName < b.englishName) {
      return -1;
    }

    if (a.englishName > b.englishName) {
      return 1;
    }

    return 0;
  }); */

  // put items begin with first term to top
  result.sort((a: MagicCard, b: MagicCard) => {
    let ia = a.englishName.toLowerCase().indexOf(firstTerm);
    let ib = b.englishName.toLowerCase().indexOf(firstTerm);

    ia = ia === -1 ? 99 : ia;
    ib = ib === -1 ? 99 : ib;

    return ia - ib;
  });

  // console.log('SEARCH RESULT BY TEXT', result);

  return result;
};

const fetchCardByName = (englishName: string): MagicCard | null => {
  const result = collectionCards.find({ englishName });

  if (result && result.length > 0) {
    appendFields(result[0]);
    // console.log('SEARCH RESULT BY NAME', result);

    return result[0];
  }

  return null;
};

const fetchCardById = (id: string): MagicCard | null => {
  const result = collectionCards.find({ id });

  if (result && result.length > 0) {
    appendFields(result[0]);
    // console.log('SEARCH RESULT BY ID', result);

    return result[0];
  }

  return null;
};

const fetchCardByOracleId = (oracleId: string): MagicCard | null => {
  const result = collectionCards.find({ oracleId });

  if (result && result.length > 0) {
    appendFields(result[0]);
    // console.log('SEARCH RESULT BY ORACLE ID', result);

    return result[0];
  }

  return null;
};

const fetchCardsByEdition = (editionCode: string): MagicCard[] => {
  const result = collectionCards.find({ editionCode });

  if (result) {
    result.map(appendFields);
  }

  // console.log('SEARCH RESULT BY EDITION', result);

  return result;
};

const fetchCardByCounter = (counter: number): MagicCard | null => {
  const result = collectionCards.find({ counter });

  if (result && result.length > 0) {
    appendFields(result[0]);

    return result[0];
  }

  return null;
};

const fetchTags = (): MagicTag[] => {
  // filter unicorns
  const result = collectionTags.find({ ref_count: { $gte: 4 } });
  const newResult: MagicTag[] = [];

  for (let i = 0; i < result.length; i++) {
    const tag = result[i];
    const name = formatTag(tag.name);
    const type = tag.type;
    newResult.push(new MagicTag(name, type, true));
  }

  // add all colors
  for (const s in COLOR_NAMES) {
    newResult.push(new MagicTag(COLOR_NAMES[s], TAG_TYPES.COLOR, true));
  }

  return newResult;
};

const addCardToIndex = (obj: MagicCard): void => {
  // create auto tags
  obj.superTypes.forEach((name: string) => {
    try {
      collectionTags.insert({ name, type: TAG_TYPES.CARD_TYPE, ref_count: 1 });
    } catch (e) {
      const res = collectionTags.find({ name });
      res[0].ref_count++;
      collectionTags.update(res[0]);
    }
  });
  obj.subTypes.forEach((name: string) => {
    try {
      collectionTags.insert({ name, type: TAG_TYPES.TRIBAL, ref_count: 1 });
    } catch (e) {
      const res = collectionTags.find({ name });
      res[0].ref_count++;
      collectionTags.update(res[0]);
    }
  });
  obj.keywords.forEach((name: string) => {
    try {
      collectionTags.insert({ name, type: TAG_TYPES.KEYWORD, ref_count: 1 });
    } catch (e) {
      const res = collectionTags.find({ name });
      res[0].ref_count++;
      collectionTags.update(res[0]);
    }
  });

  obj.counter = cardsCounter++;
  collectionCards.insert(obj);
};

const updateCardInIndex = (obj: MagicCard): void => {
  collectionCards.update(obj);
};

const countTotalCards = (): number => {
  return collectionCards.data.length;
};

export default {
  initIndex,
  saveIndexDatabase,
  searchCardByText,
  fetchCardByName,
  fetchCardById,
  fetchCardByOracleId,
  fetchCardByCounter,
  addCardToIndex,
  updateCardInIndex,
  countTotalCards,
  fetchCardsByEdition,
  fetchTags
};
