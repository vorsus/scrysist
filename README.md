# Welcome to Scrysist!

Scrysist is an unofficial desktop app to manage your decks for Magic: The Gathering ("MTG" Trading Card Game).
It works mostly offline and stores all your data locally in plain (json) files. Made for players that do not want to use ad-heavy online services. Scrysist is designed for Paper Magic, "online only" cards are not handled by design.

This app is inspired by [Magic Assistant](https://sourceforge.net/projects/mtgbrowser/) by Alena Laskavaia.

The program is build with electron, so it is easy to support multiple operating systems, but it is less lightweight. At least a Full HD resolution is recommended.

## Download

Please go to the release page and get the most up to date version for your operating system, just unpack it and start brewing!

## First usage

On first start, the app downloads some cards data (approx. 500 MB) and builds an index. This takes some while, but don't worry, it is only needed once (for each new database). You can update the database any time in the settings. You also can create some example decks from the file tree.

Each card displayed for the first time will download the card face image and cache it on your pc. Be sure you have some GB space left to benefit from that.

# Support

When you like this app, feel free to gift me a deck box or some sleeves from <a rel="noreferrer noopener" target="_blank" href="https://www.amazon.de/hz/wishlist/ls/O1QT2WT9HSWW">my amazon wish list</a>.

Found an issue? Please use the issue tracker here on the codeberg repository. I speak german and english. I am also happy to hear some kind words. :)

In the settings you can access the debug console for some logging information.

# Credits

Thanks to this great projects used by Scrysist:
[ElectronJS](https://www.electronjs.org), [VueJS](https://vuejs.org/), [ViteJS](https://vitejs.dev/), [PrimeVue](https://primevue.org/), [PrimeFlex](https://www.primefaces.org/primeflex/), [LokiJS](https://github.com/techfort/LokiJS), [Simple-JSONdb](https://github.com/nmaggioni/Simple-JSONdb), [ApexCharts](https://apexcharts.com), [Mana Font](https://github.com/andrewgioia/Mana) and more...

# License

Literal and graphical information, images and a portion of the related icons and symbols about Magic: The Gathering are provided by [Scryfall.com](https://scryfall.com) free of charge under [Wizards of the Coast's Fan Content Policy](https://company.wizards.com/en/legal/fancontentpolicy).

Scrysist is distributed under AGPL-3.0:

Copyright (C) 2023 Vorsus

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.
